<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Helpers\MiscFunctions;

#[CoversClass(MiscFunctions::class)]
class MiscFunctionsTest extends TestCase
{
    public static string $continue_result;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass(); // TODO: Change the autogenerated stub

        $dirName = realpath(__DIR__);
        if ($dirName !== false) {
            set_include_path(get_include_path() . PATH_SEPARATOR . $dirName);
        }
    }

    public function testDebugPrint()
    {
        ob_start();
        MiscFunctions::DebugPrint('this is the test string');
        $result = ob_get_clean();

        if (defined('DEBUG'))
            $this->assertEquals('this is the test string', $result);
        else
            $this->assertEquals('', $result);
    }

    public function testDoPostRequest()
    {
        // expect success
        $url = 'https://httpbin.org/post';
        $data = 'name1=value1&name2=value2';
        $result = MiscFunctions::DoPostRequest($url, $data);
        $this->assertNotNull($result);

        $obj = json_decode($result);
        $this->assertEquals('value1', $obj->form->name1);

        // expect 404
        $url = 'https://httpbin.org/status/404';
        ob_start();
        $result = MiscFunctions::DoPostRequest($url, $data);
        $output = ob_get_clean();

        $this->assertFalse($result);
        $this->assertStringStartsWith('Problem with', $output);
    }

    public function testAjaxStringToArray()
    {
        $input = [
            "col1\tcol2\tcol3",
            "val1-1\tval1-2\tval1-3",
            "val2-1\tval2-2\tval2-3",
            "val3-1\tval3-2\tval3-3",
        ];

        $output = MiscFunctions::AjaxStringToArray(implode("\r\n", $input));

        $this->assertIsArray($output);
        $this->assertCount(3, $output);
        $this->assertEquals('val3-3', $output[2]['col3']);

        $output = MiscFunctions::AjaxStringToArray("col1\tcol2\tcol3\r\n");
        $this->assertNull($output);
    }

    public function testFileExistsInPath()
    {
        $this->assertTrue(MiscFunctions::FileExistsInPath('MiscFunctionsTest.php'));
        $this->assertFalse(MiscFunctions::FileExistsInPath('does-not-exist.conf'));
    }

    public function testGetFilenameInPath()
    {
        $expect = realpath(__FILE__);
        $this->assertEquals($expect, MiscFunctions::GetFilenameInPath('MiscFunctionsTest.php'));
        $this->assertFalse(MiscFunctions::GetFilenameInPath('does-not-exist.conf'));
    }

    public function testEvalPhpFile()
    {
        // test legit file
        $file = TEST_RESOURCES . '/eval-test.php';
        $result = MiscFunctions::EvalPhpFile($file);
        $this->assertEquals('eval test', $result);

        // test bogus file
        $file = TEST_RESOURCES . '/does-not-exist.php';
        $result = MiscFunctions::EvalPhpFile($file);
        $this->assertEquals('', $result);
    }

    public function testParseSize()
    {
        $this->assertEquals(1, MiscFunctions::ParseSize('1'));
        $this->assertEquals(1, MiscFunctions::ParseSize('1B'));
        $this->assertEquals(1024, MiscFunctions::ParseSize('1K'));
        $this->assertEquals(1024 ** 2, MiscFunctions::ParseSize('1M'));
        $this->assertEquals(1024 ** 3, MiscFunctions::ParseSize('1G'));
        $this->assertEquals(1024 ** 4, MiscFunctions::ParseSize('1T'));
        $this->assertEquals(1024 ** 5, MiscFunctions::ParseSize('1P'));
    }

    public function testGetMaxUploadFileSize()
    {
        $this->assertGreaterThan(0, MiscFunctions::GetMaxUploadFileSize());
    }

    public function testYamlDecodeFile()
    {
        $file = TEST_RESOURCES . '/test.yaml';
        $obj = MiscFunctions::YamlDecodeFile($file);

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj['database']);
        $this->assertNotEmpty($obj['database']['server']);
        $this->assertEquals('db.example.com', $obj['database']['server']);

        $obj = MiscFunctions::YamlDecodeFile($file, false);

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->database);
        $this->assertNotEmpty($obj->database->database);
        $this->assertEquals('test_db', $obj->database->database);
    }

    public function testTrimArray()
    {
        $a = [
            ' 1 ',
            '     2',
            "\r\n3\r\n\t"
        ];

        $this->assertNotEquals(['1', '2', '3'], $a);

        MiscFunctions::TrimArray($a);

        $this->assertEquals(['1', '2', '3'], $a);
    }

    public function testArrayIsAssociative()
    {
        $result = MiscFunctions::ArrayIsAssociative([]);
        $this->assertFalse($result);

        $result = MiscFunctions::ArrayIsAssociative(['a', 'b', 'c']);
        $this->assertFalse($result);

        $result = MiscFunctions::ArrayIsAssociative(['a' => 'aa', 'b', 'c']);
        $this->assertTrue($result);
    }

    public function testArrayToObject()
    {
        $a = ['a' => 1, 'b' => 2];
        $o = MiscFunctions::ArrayToObject($a);
        $this->assertEquals(1, $o->a);
        $this->assertEquals(2, $o->b);

        $a = [['a' => 1], ['a' => 2]];
        $o = MiscFunctions::ArrayToObject($a);
        $this->assertEquals(1, $o[0]->a);
        $this->assertEquals(2, $o[1]->a);
    }

    public function testSendAndContinue()
    {
        self::$continue_result = 'Did not run';
        $this->assertEquals('Did not run', MiscFunctionsTest::$continue_result);

        MiscFunctions::SendAndContinue(function () {
            MiscFunctionsTest::$continue_result = 'Did run';
        });

        $this->assertEquals('Did run', MiscFunctionsTest::$continue_result);
    }

    public function testClearDirectory()
    {
        $test_dir = '/tmp/clear-dir-test';
        $sub_dir = $test_dir . '/sub-dir';
        $test_file = $test_dir . '/test.txt';

        if (!is_dir($test_dir))
            mkdir($test_dir, 0777, true);

        $this->assertDirectoryExists($test_dir);

        if (!is_dir($sub_dir))
            mkdir($sub_dir, 0777, true);

        $this->assertDirectoryExists($sub_dir);

        file_put_contents($test_file, 'this is the text');
        $this->assertFileExists($test_file);

        MiscFunctions::ClearDirectory($test_dir, true);

        $this->assertFileDoesNotExist($test_file);
        $this->assertDirectoryDoesNotExist($sub_dir);
        $this->assertDirectoryDoesNotExist($test_dir);
    }
}
