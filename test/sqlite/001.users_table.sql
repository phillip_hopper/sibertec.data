create table users
(
    id integer primary key autoincrement,
    user_name varchar(50) null,
    user_pwd varchar(50) null,
    created_at timestamp default current_timestamp not null
);

insert into users (id, user_name, user_pwd)
values (1, 'User One', 'password-one'),
       (2, 'User Two', 'password-two'),
       (3, 'User Three', 'password-three');
