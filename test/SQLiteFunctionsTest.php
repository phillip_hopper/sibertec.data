<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Helpers\SQLiteFunctions;

#[CoversClass(SQLiteFunctions::class)]
class SQLiteFunctionsTest extends TestCase
{
    public function setUp(): void
    {
        date_default_timezone_set('UTC');
        parent::setUp();
    }

    public function test_CONCAT()
    {
        $this->assertEquals('12b', SQLiteFunctions::CONCAT(1, '2', 'b'));
        $this->assertNull(SQLiteFunctions::CONCAT(1, null, 'b'));
    }

    public function test_IF_function()
    {
        $this->assertEquals('a', SQLiteFunctions::IF_function(1 < 2, 'a', 'b'));
        $this->assertEquals('b', SQLiteFunctions::IF_function(1 > 2, 'a', 'b'));
    }

    public function test_CONCAT_WS()
    {
        $this->assertEquals('1,2,b', SQLiteFunctions::CONCAT_WS(',', 1, '2', 'b'));
        $this->assertEquals('1,b', SQLiteFunctions::CONCAT_WS(',', 1, null, 'b'));
        $this->assertNull(SQLiteFunctions::CONCAT_WS(null, 1, '2', 'b'));
    }

    public function test_DATEDIFF()
    {
        $this->assertEquals(4, SQLiteFunctions::DATEDIFF('2023-02-01', '2023-01-28'));
        $this->assertEquals(1, SQLiteFunctions::DATEDIFF('2023-02-01', '2023-01-31'));
        $this->assertEquals(-4, SQLiteFunctions::DATEDIFF('2023-01-28', '2023-02-01'));
        $this->assertEquals(365, SQLiteFunctions::DATEDIFF(1699156800, 1667620800));
        $this->assertNull(SQLiteFunctions::DATEDIFF(1, null));
    }

    public function test_DAYOFWEEK()
    {
        $this->assertEquals(1, SQLiteFunctions::DAYOFWEEK('2023-11-19'));
        $this->assertEquals(7, SQLiteFunctions::DAYOFWEEK('2023-11-25'));
        $this->assertEquals(1, SQLiteFunctions::DAYOFWEEK('2023-11-26'));
        $this->assertNull(SQLiteFunctions::DAYOFWEEK(null));
    }

    public function test_FROM_UNIXTIME()
    {
        $this->assertEquals('2023-11-05 04:00:00', SQLiteFunctions::FROM_UNIXTIME(1699156800));
        $this->assertEquals('2023-11-05 00:00:00', SQLiteFunctions::FROM_UNIXTIME('2023-11-05'));
    }

    public function test_GREATEST()
    {
        $this->assertEquals(8, SQLiteFunctions::GREATEST(1, 2, 4, 8));
        $this->assertEquals(22, SQLiteFunctions::GREATEST(1, 22, 4, 8));
        $this->assertNull(SQLiteFunctions::GREATEST(1, null, 4, 8));
    }

    public function test_NOW()
    {
        $now = SQLiteFunctions::NOW();
        $this->assertMatchesRegularExpression('/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/', $now);
    }

    public function test_REGEXP()
    {
        $this->assertEquals(1, SQLiteFunctions::REGEXP('[0-9]', '1234567890'));
        $this->assertEquals(0, SQLiteFunctions::REGEXP('[0-9]', 'abcdefghij'));
        $this->assertNull(SQLiteFunctions::REGEXP(null, 'abcdefghij'));
        $this->assertNull(SQLiteFunctions::REGEXP('[0-9]', null));
    }

    public function test_SUBSTRING_INDEX()
    {
        $this->assertEquals('www.mysql', SQLiteFunctions::SUBSTRING_INDEX('www.mysql.com', '.', 2));
        $this->assertEquals('mysql.com', SQLiteFunctions::SUBSTRING_INDEX('www.mysql.com', '.', -2));
        $this->assertNull(SQLiteFunctions::SUBSTRING_INDEX(null, '.', -2));
        $this->assertNull(SQLiteFunctions::SUBSTRING_INDEX('www.mysql.com', null, -2));
        $this->assertNull(SQLiteFunctions::SUBSTRING_INDEX('www.mysql.com', '.', null));
    }

    public function test_TRUNCATE()
    {
        $this->assertEquals(3.3333, SQLiteFunctions::TRUNCATE(10 / 3, 4));
        $this->assertNull(SQLiteFunctions::TRUNCATE(null, 4));
        $this->assertNull(SQLiteFunctions::TRUNCATE(10 / 3, null));
    }

    public function test_YEAR()
    {
        $this->assertEquals('2018', SQLiteFunctions::YEAR('2018-06-28'));
    }

    public function test_MONTH()
    {
        $this->assertEquals('6', SQLiteFunctions::MONTH('2018-06-28'));
    }

    public function test_CURDATE()
    {
        $date = SQLiteFunctions::CURDATE();
        $this->assertMatchesRegularExpression('/\d{4}-\d{2}-\d{2}/', $date);
        $this->assertEquals($date, SQLiteFunctions::CURRENT_DATE());
    }

    public function test_FIND_IN_SET()
    {
        $this->assertNull(SQLiteFunctions::FIND_IN_SET(null, '1,2,3,4,5'));
        $this->assertNull(SQLiteFunctions::FIND_IN_SET(3, null));
        $this->assertEquals(0, SQLiteFunctions::FIND_IN_SET('', '1,2,3,4,5'));
        $this->assertEquals(0, SQLiteFunctions::FIND_IN_SET('3', ''));
        $this->assertEquals(3, SQLiteFunctions::FIND_IN_SET('3', '1,2,3,4,5'));
        $this->assertEquals(2, SQLiteFunctions::FIND_IN_SET(2, '1,2,3,4,5'));
    }

    public function test_CHAR_LENGTH()
    {
        $this->assertNull(SQLiteFunctions::CHAR_LENGTH(null));
        $this->assertEquals(6, SQLiteFunctions::CHAR_LENGTH('qwerty'));
    }

    public function testCOMPRESS()
    {
        $this->assertEquals('this_is_a_test', SQLiteFunctions::COMPRESS('this_is_a_test'));
    }

    public function testUNCOMPRESS()
    {
        $this->assertEquals('this_is_a_test', SQLiteFunctions::UNCOMPRESS('this_is_a_test'));
    }

    public function testTO_BASE64()
    {
        $this->assertEquals('dGhpc19pc19hX3Rlc3Q=', SQLiteFunctions::TO_BASE64('this_is_a_test'));
    }

    public function test_RAND()
    {
        $int = SQLiteFunctions::RAND();
        $this->assertGreaterThan(0, $int);
    }
}
