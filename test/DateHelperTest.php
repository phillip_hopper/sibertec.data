<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Helpers\DateHelper;

date_default_timezone_set('America/New_York');

#[CoversClass(DateHelper::class)]
class DateHelperTest extends TestCase
{
    public function testToISO8601Zulu()
    {
        $val = DateHelper::ToISO8601Zulu(1012616542);
        $this->assertEquals('2002-02-02T02:22:22Z', $val);

        $val = DateHelper::ToISO8601Zulu(1012659742);
        $this->assertEquals('2002-02-02T14:22:22Z', $val);

        $val = DateHelper::ToISO8601Zulu(null);
        $this->assertNull($val);
    }

    public function testISO8601ZuluToSQL()
    {
        $val = DateHelper::DateTimeStrToSQL('2002-02-02T02:22:22Z');

        $this->assertEquals('2002-02-01 21:22:22', $val);

        $val = DateHelper::DateTimeStrToSQL('02/02/2002 2:22:22PM');

        $this->assertEquals('2002-02-02 14:22:22', $val);
    }

    public function testGetWebinarDate()
    {
        $year = date('Y');

        // test today
        $val = DateHelper::GetWebinarDate(date('D j, M') . ' at 12:00 PM');
        $this->assertEquals(strtotime('today'), $val);

        // tomorrow should return the same date last year, not a future date
        $val = DateHelper::GetWebinarDate(date('D j, M', strtotime('tomorrow')) . ' at 12:00 PM');
        $expect = strtotime(($year - 1) . '-' . date('m-d', strtotime('tomorrow')));
        $this->assertEquals($expect, $val);
    }

    public function testGetWebinarMinutes()
    {
        $val = DateHelper::GetWebinarMinutes('01:00:00');
        $this->assertEquals(60, $val);

        $val = DateHelper::GetWebinarMinutes('01:00:29');
        $this->assertEquals(60, $val);

        $val = DateHelper::GetWebinarMinutes('01:00:30');
        $this->assertEquals(61, $val);

        $val = DateHelper::GetWebinarMinutes('00:59:29');
        $this->assertEquals(59, $val);

        $val = DateHelper::GetWebinarMinutes('59:29');
        $this->assertEquals(0, $val);
    }

    public function testGetTimezoneOffset()
    {
        $val = DateHelper::GetTimezoneOffset();
        $this->assertNotNull($val);
        $this->assertMatchesRegularExpression('/[+-]+\d\d:\d\d/', $val);
    }

    public function testIsDST()
    {
        $val = DateHelper::IsDST();
        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->assertTrue(is_bool($val));
    }

    public function testDateTimeStrToSQLBadValues()
    {
        $val = DateHelper::DateTimeStrToSQL(null);
        $this->assertNull($val);

        $val = DateHelper::DateTimeStrToSQL(-1);
        $this->assertNull($val);

        $val = DateHelper::DateTimeStrToSQL(false);
        $this->assertNull($val);
    }

    public function testDateStrToSQL()
    {
        $val = DateHelper::DateStrToSQL('12/31/2020');
        $this->assertEquals('2020-12-31', $val);

        $val = DateHelper::DateStrToSQL(-1);
        $this->assertNull($val);
    }

    public function testToShortDateString()
    {
        $val = DateHelper::ToShortDateString('2020-12-31 23:59');
        $this->assertEquals('12/31/2020', $val);

        $val = DateHelper::ToShortDateString(null);
        $this->assertEquals('', $val);
    }

    public function testToShortDateTimeString()
    {
        $val = DateHelper::ToShortDateTimeString('2020-12-31 23:59');
        $this->assertEquals('12/31/2020 11:59 PM', $val);

        $val = DateHelper::ToShortDateTimeString(null);
        $this->assertEquals('', $val);
    }

    public function testCurrentISOTimestamp()
    {
        $val = DateHelper::CurrentISOTimestamp();
        $this->assertMatchesRegularExpression('/^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d[+-]+\d\d:\d\d/', $val);
    }

    public function testUTC_to_EST()
    {
        $val = DateHelper::UTC_to_EST('2020-12-01 12:00:00');
        $this->assertEquals('2020-12-01 7:00:00 AM EST', $val);

        $val = DateHelper::UTC_to_EST('2021-07-01 12:00:00');
        $this->assertEquals('2021-07-01 8:00:00 AM EDT', $val);
    }

    public function testDateDiff()
    {
        $val = DateHelper::DateDiff(strtotime('2020-01-01'), strtotime('2020-01-03'));
        $this->assertEquals(2, $val);

        $val = DateHelper::DateDiff(strtotime('2020-01-04'), strtotime('2020-01-01'));
        $this->assertEquals(-3, $val);
    }

    public function testWeekDiff()
    {
        $val = DateHelper::WeekDiff(strtotime('2020-01-01'), strtotime('2020-01-31'));
        $this->assertEquals(4, $val);

        $val = DateHelper::WeekDiff(strtotime('2020-02-06'), strtotime('2020-01-01'));
        $this->assertEquals(-5, $val);
    }

    public function testFirstDayOfMonth()
    {
        $val = DateHelper::FirstDayOfMonth('12/31/2020');
        $this->assertEquals(strtotime('2020-12-01'), $val);
    }

    public function testLastDayOfMonth()
    {
        $val = DateHelper::LastDayOfMonth('12/20/2020');
        $this->assertEquals(strtotime('2020-12-31 23:59:59'), $val);
    }

    public function testFirstDayOfWeek()
    {
        $val = DateHelper::FirstDayOfWeek('12/25/2020');
        $this->assertEquals(strtotime('2020-12-20'), $val);
    }

    public function testLastDayOfWeek()
    {
        $val = DateHelper::LastDayOfWeek('12/25/2020');
        $this->assertEquals(strtotime('2020-12-26'), $val);
    }

    public function testSec2HMS()
    {
        $val = DateHelper::Sec2HMS(1000);
        $this->assertEquals('0:16:40', $val);

        $val = DateHelper::Sec2HMS(1);
        $this->assertEquals('0:00:01', $val);

        $val = DateHelper::Sec2HMS(46967);
        $this->assertEquals('13:02:47', $val);
    }

    public function testMinutesToTimeStr()
    {
        $val = DateHelper::MinutesToTimeStr(0);
        $this->assertEquals('12:00 AM', $val);

        $val = DateHelper::MinutesToTimeStr(480);
        $this->assertEquals('8:00 AM', $val);

        $val = DateHelper::MinutesToTimeStr(720);
        $this->assertEquals('12:00 PM', $val);

        $val = DateHelper::MinutesToTimeStr(780);
        $this->assertEquals('1:00 PM', $val);

        $val = DateHelper::MinutesToTimeStr(780, false);
        $this->assertEquals('13:00', $val);
    }
}
