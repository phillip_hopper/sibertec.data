<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Controllers\AppData;
use Sibertec\Data\Helpers\SqlHelper;

#[CoversClass(SqlHelper::class)]
class SqlHelperTest extends TestCase
{
    public function testLoadSQL()
    {
        $file_name = 'get_user_list.sql';
        $db = AppData::MainDatabase();
        $sql = SqlHelper::LoadSQL($file_name, $db);
        $this->assertNotEmpty($sql);

        $file_name = TEST_RESOURCES . '/get_user_list.sql';
        $sql2 = SqlHelper::LoadSQL($file_name, $db);
        $this->assertNotEmpty($sql2);

        if (!defined('SQL_DIR'))
            define('SQL_DIR', TEST_RESOURCES);
        $file_name = 'get_user_list.sql';
        $sql = SqlHelper::LoadSQL($file_name, $db);
        $this->assertNotEmpty($sql);

        $file_name = 'does-not-exist.sql';
        $sql = SqlHelper::LoadSQL($file_name, $db);
        $this->assertEmpty($sql);
    }

    public function testPrepare()
    {
        $sql = 'SELECT * FROM rvw_com.`user_role`';
        $db = AppData::MainDatabase();

        $sql2 = SqlHelper::Prepare($db, $sql, []);
        $this->assertEquals('SELECT * FROM rvw_com.`user_role`', $sql2);

        $sql = "SELECT * FROM rvw_com.`user_role` WHERE id = '@id'";
        $sql3 = SqlHelper::Prepare($db, $sql, ['@id' => '1']);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = '1'", $sql3);

        $sql7 = SqlHelper::Prepare($db, $sql, ['@id' => null]);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = NULL", $sql7);

        $sql = "SELECT * FROM rvw_com.`user_role` WHERE id = '" . PHP_EOL . "@id'";
        $sql4 = SqlHelper::Prepare($db, $sql, ['@id' => '1']);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = '\n1'", $sql4);

        $sql5 = SqlHelper::Prepare($db, $sql, ['@id' => '1'], true);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = '\n1'", $sql5);

        $sql = "SELECT * FROM rvw_com.`user_role` WHERE id = %s";
        $sql6 = SqlHelper::Prepare($db, $sql, ['2']);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = '2'", $sql6);

        $sql8 = SqlHelper::Prepare($db, $sql, [null]);
        $this->assertEquals("SELECT * FROM rvw_com.`user_role` WHERE id = NULL", $sql8);
    }

    public function testSqlFileToArray()
    {
        $file_name = 'get_user_list.sql';
        $db = AppData::MainDatabase();
        $rows = SqlHelper::SqlFileToArray($file_name, $db);
        $this->assertEquals(3, count($rows));

        $sql = SqlHelper::LoadSQL($file_name, $db);
        $rows2 = SqlHelper::SqlFileToArray($sql, $db, [], false, true);
        $this->assertGreaterThan(0, count($rows2));
        $this->assertEquals(1, $rows2[0]->row_num);
    }

    public function testSqlFileToScalarObject()
    {
        $file_name = 'get_user_list.sql';
        $db = AppData::MainDatabase();
        $row = SqlHelper::SqlFileToScalarObject($file_name, $db);
        $this->assertNotEmpty($row);
        $this->assertGreaterThan(-1, $row->id);
    }

    public function testFloorplanDateToSQL()
    {
        $result = SqlHelper::FloorplanDateToSQL('11-05-1988');
        $this->assertEquals('1988-11-05', $result);

        $result = SqlHelper::FloorplanDateToSQL('5-11-1988');
        $this->assertEquals('1988-05-11', $result);

        $result = SqlHelper::FloorplanDateToSQL('05-11-1988');
        $this->assertEquals('1988-05-11', $result);
    }

    public function testProcessIncludes()
    {
        if (!defined('SQL_DIR'))
            define('SQL_DIR', TEST_RESOURCES);

        $file_name = 'includes_another.sql';
        $db = AppData::MainDatabase();
        $sql = SqlHelper::LoadSQL($file_name, $db);

        /** @noinspection SqlResolve */
        $expected = <<<'SQL'
SELECT * FROM tbl

WHERE 1=1
UNION
SELECT * FROM tbl
SQL;
        $this->assertEquals($expected, $sql);
    }

    public function testSqlFileToAssociativeArray()
    {
        $file_name = 'get_user_list.sql';
        $db = AppData::MainDatabase();
        $rows = SqlHelper::SqlFileToAssociativeArray($file_name, $db);

        $this->assertIsArray($rows);
        $this->assertCount(3, $rows);

        $this->assertIsArray($rows[0]);
        $this->assertEquals(1, $rows[0]['id']);
    }
}
