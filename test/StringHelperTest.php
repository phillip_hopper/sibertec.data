<?php

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Helpers\StringHelper;

#[CoversClass(StringHelper::class)]
class StringHelperTest extends TestCase
{
	public function testFormatPhone()
    {
		$value1 = '1234567890';
		$value2 = '1234567';
		$value3 = '123-456-7890';
		$value4 = '123456789';
		$value5 = 'something';
        $value6 = '11234567890';

		$this->assertEquals('(123) 456-7890', StringHelper::FormatPhone($value1));
		$this->assertEquals('123-4567', StringHelper::FormatPhone($value2));
		$this->assertEquals('(123) 456-7890', StringHelper::FormatPhone($value3));
        $this->assertEquals('123-456-7890', StringHelper::FormatPhone($value3, false));
		$this->assertEquals($value4, StringHelper::FormatPhone($value4));
		$this->assertEquals($value5, StringHelper::FormatPhone($value5));
        $this->assertEquals('(123) 456-7890', StringHelper::FormatPhone($value6));
	}

    public function testFormatPhoneSMS()
    {
        $value1 = '1234567890';
        $value2 = '123-4567';
        $value3 = '123-456-7890';
        $value4 = '123456789';
        $value5 = 'something';
        $value6 = '11234567890';

        $this->assertEquals('+11234567890', StringHelper::FormatPhoneSMS($value1));
        $this->assertEquals('1234567', StringHelper::FormatPhoneSMS($value2));
        $this->assertEquals('+11234567890', StringHelper::FormatPhoneSMS($value3));
        $this->assertEquals('11234567890', StringHelper::FormatPhoneSMS($value3, false));
        $this->assertEquals($value4, StringHelper::FormatPhoneSMS($value4));
        $this->assertEquals('', StringHelper::FormatPhoneSMS($value5));
        $this->assertEquals('+11234567890', StringHelper::FormatPhoneSMS($value6));
    }

    public function testFormatPhoneRegex()
    {
        $value1 = '1234567890';
        $value2 = '123-4567';
        $value3 = '123-456-7890';
        $value4 = '123456789';
        $value5 = 'something';
        $value6 = '11234567890';

        $this->assertEquals('.*123.*456.*7890', StringHelper::FormatPhoneRegex($value1));
        $this->assertNull(StringHelper::FormatPhoneRegex($value2));
        $this->assertEquals('.*123.*456.*7890', StringHelper::FormatPhoneRegex($value3));
        $this->assertNull(StringHelper::FormatPhoneRegex($value4));
        $this->assertNull(StringHelper::FormatPhoneRegex($value5));
        $this->assertEquals('.*123.*456.*7890', StringHelper::FormatPhoneRegex($value6));
    }

	public function testBeginsWith()
    {
        $this->assertTrue(StringHelper::BeginsWith('asdfghjkl', 'asdf'));
        $this->assertTrue(StringHelper::BeginsWith('asdf', 'asdf'));
        $this->assertFalse(StringHelper::BeginsWith('asdfghjkl', 'qwer'));
        $this->assertFalse(StringHelper::BeginsWith('asdfghjkl', ''));
        $this->assertFalse(StringHelper::BeginsWith('', 'qwer'));
        $this->assertFalse(StringHelper::BeginsWith('', ''));
    }

    public function testBeginsWithAny()
    {
        $this->assertTrue(StringHelper::BeginsWithAny('asdfghjkl', ['qwer', 'asdf', 'zxcv']));
        $this->assertFalse(StringHelper::BeginsWithAny('qasdfghjkl', ['qwer', 'asdf', 'zxcv']));
        $this->assertFalse(StringHelper::BeginsWithAny('', ['qwer', 'asdf', 'zxcv']));
        $this->assertFalse(StringHelper::BeginsWithAny('asdfghjkl', []));
        $this->assertFalse(StringHelper::BeginsWithAny('', []));
    }

    public function testEndsWith()
    {
        $this->assertTrue(StringHelper::EndsWith('ghjklasdf', 'asdf'));
        $this->assertTrue(StringHelper::EndsWith('asdf', 'asdf'));
        $this->assertFalse(StringHelper::EndsWith('ghjklasdf', 'qwer'));
        $this->assertFalse(StringHelper::EndsWith('ghjklasdf', ''));
        $this->assertFalse(StringHelper::EndsWith('', 'qwer'));
        $this->assertFalse(StringHelper::EndsWith('', ''));
    }

    public function testContains()
    {
        $this->assertTrue(StringHelper::Contains('ghjklasdfghjkl', 'asdf'));
        $this->assertTrue(StringHelper::Contains('asdfghjkl', 'asdf'));
        $this->assertFalse(StringHelper::Contains('ghjklasdfghjkl', 'qwer'));
        $this->assertTrue(StringHelper::Contains('ghjklasdf', 'asdf'));
        $this->assertTrue(StringHelper::Contains('asdf', 'asdf'));
        $this->assertFalse(StringHelper::Contains('ghjklasdf', 'qwer'));
        $this->assertTrue(StringHelper::Contains('asdfghjkl', 'asdf'));
        $this->assertTrue(StringHelper::Contains('asdf', 'asdf'));
        $this->assertFalse(StringHelper::Contains('asdfghjkl', 'qwer'));
        $this->assertFalse(StringHelper::Contains('asdfghjkl', ''));
        $this->assertFalse(StringHelper::Contains('', 'qwer'));
        $this->assertFalse(StringHelper::Contains('', ''));
    }

    public function testContainsAny()
    {
        $this->assertTrue(StringHelper::ContainsAny('ghjklasdfghjkl', ['asdf', 'fdsa']));
        $this->assertFalse(StringHelper::ContainsAny('ghjklasdfghjkl', ['abcd', 'fdsa']));
        $this->assertFalse(StringHelper::ContainsAny('ghjklasdfghjkl', ['']));
        $this->assertFalse(StringHelper::ContainsAny('', ['']));
    }

    public function testPathCombine()
    {
        $parts = explode(PATH_SEPARATOR, __FILE__);
        $found = call_user_func_array('Sibertec\Data\Helpers\StringHelper::PathCombine', [$parts]);
        $this->assertFileEquals(realpath(__FILE__), $found);
    }

    public function testRealPathCombine()
    {
        $parts = explode(PATH_SEPARATOR, __FILE__);
        $found = call_user_func_array('Sibertec\Data\Helpers\StringHelper::RealPathCombine', $parts);
        $this->assertFileEquals(realpath(__FILE__), $found);
    }

    public function testUrlCombine()
    {
        $this->assertEquals('/localhost/path1/path2/path3/img.jp', StringHelper::UrlCombine('/localhost/', '/path1/', 'path2', 'path3', 'img.jp'));
    }

    public function testGUID()
    {
        $guid = StringHelper::GUID();
        $this->assertNotEmpty($guid);
    }

    public function testLowerEscape()
    {
        $in = 'Rockwood Mini Lite 2109S by Forest River #12345';
        $out = StringHelper::LowerEscape($in);
        $this->assertEquals('rockwood+mini+lite+2109s+by+forest+river+%2312345', $out);

        $out = StringHelper::LowerDashEscape($in);
        $this->assertEquals('rockwood-mini-lite-2109s-by-forest-river-%2312345', $out);
    }

    public function testPluralize()
    {
        $this->assertEquals('dog', StringHelper::Pluralize(1, 'dog'));
        $this->assertEquals('', StringHelper::Pluralize(2, '', 'dogies'));
        $this->assertEquals('dogs', StringHelper::Pluralize(2, 'dog'));
        $this->assertEquals('dogies', StringHelper::Pluralize(2, 'dog', 'dogies'));
        $this->assertEquals('dogies', StringHelper::Pluralize(2, 'dogy'));
        $this->assertEquals('dogs', StringHelper::Pluralize(2, 'dogs'));
    }

    public function testSingularize()
    {
        $result = StringHelper::Singularize('boxes');
        $this->assertEquals('box', $result);

        $result = StringHelper::Singularize('deer');
        $this->assertEquals('deer', $result);

        $result = StringHelper::Singularize('toy haulers');
        $this->assertEquals('toy hauler', $result);

        $result = StringHelper::Singularize('motorhomes');
        $this->assertEquals('motorhome', $result);

        $result = StringHelper::Singularize('RVs');
        $this->assertEquals('RV', $result);

        $result = StringHelper::Singularize('berries');
        $this->assertEquals('berry', $result);

        $result = StringHelper::Singularize('knives');
        $this->assertEquals('knife', $result);

        $result = StringHelper::Singularize('scarves');
        $this->assertEquals('scarf', $result);

        $result = StringHelper::Singularize('men');
        $this->assertEquals('man', $result);

        $result = StringHelper::Singularize('women');
        $this->assertEquals('woman', $result);

        $result = StringHelper::Singularize('illuminatae');
        $this->assertEquals('illuminatae', $result);
    }

    public function testProperCaseName()
    {
        $result = StringHelper::ProperCaseName('mcbob mcdonald');
        $this->assertEquals('McBob McDonald', $result);

        $result = StringHelper::ProperCaseName('o\'bob o\'donald');
        $this->assertEquals("O'Bob O'Donald", $result);

        $result = StringHelper::ProperCaseName('MCBOB MCDONALD');
        $this->assertEquals('McBob McDonald', $result);

        $result = StringHelper::ProperCaseName('XLR');
        $this->assertEquals('XLR', $result);

        $result = StringHelper::ProperCaseName('toy hauler TT');
        $this->assertEquals('Toy Hauler TT', $result);
    }

    public function testIsValidEmail()
    {
        // these should fail
        $this->assertFalse(StringHelper::IsValidEmail(''));
        $this->assertFalse(StringHelper::IsValidEmail('123'));
        $this->assertFalse(StringHelper::IsValidEmail('one@mail'));
        $this->assertFalse(StringHelper::IsValidEmail('one.mail.com'));
        $this->assertFalse(StringHelper::IsValidEmail('one @mail.com'));

        // these should pass
        $this->assertTrue(StringHelper::IsValidEmail('123@mail.com'));
        $this->assertTrue(StringHelper::IsValidEmail('123.456@mail.com'));
        $this->assertTrue(StringHelper::IsValidEmail("O'Malley@mail.com"));
    }
}
