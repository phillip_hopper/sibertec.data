<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Controllers\AppData;
use Sibertec\Data\Data\SQLiteDatabase;

#[CoversClass(AppData::class)]
class AppDataTest extends TestCase
{
    public function testMainDatabase()
    {
        $db = AppData::MainDatabase();

        $this->assertNotEmpty($db);;
        $this->assertInstanceOf(SQLiteDatabase::class, $db);
    }
}
