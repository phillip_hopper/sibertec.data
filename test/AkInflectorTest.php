<?php

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Helpers\AkInflector;

#[CoversClass(AkInflector::class)]
class AkInflectorTest extends TestCase
{
    public function testPluralize()
    {
        $result = AkInflector::Pluralize('box');
        $this->assertEquals('boxes', $result);

        $result = AkInflector::Pluralize('deer');
        $this->assertEquals('deer', $result);

        $result = AkInflector::Pluralize('toy hauler');
        $this->assertEquals('toy haulers', $result);

        $result = AkInflector::Pluralize('motorhome');
        $this->assertEquals('motorhomes', $result);

        $result = AkInflector::Pluralize('RV');
        $this->assertEquals('RVs', $result);

        $result = AkInflector::Pluralize('berry');
        $this->assertEquals('berries', $result);

        $result = AkInflector::Pluralize('knife');
        $this->assertEquals('knives', $result);

        $result = AkInflector::Pluralize('man');
        $this->assertEquals('men', $result);

        $result = AkInflector::Pluralize('woman');
        $this->assertEquals('women', $result);
    }

    public function testSingularize()
    {
        $result = AkInflector::Singularize('boxes');
        $this->assertEquals('box', $result);

        $result = AkInflector::Singularize('deer');
        $this->assertEquals('deer', $result);

        $result = AkInflector::Singularize('toy haulers');
        $this->assertEquals('toy hauler', $result);

        $result = AkInflector::Singularize('motorhomes');
        $this->assertEquals('motorhome', $result);

        $result = AkInflector::Singularize('RVs');
        $this->assertEquals('RV', $result);

        $result = AkInflector::Singularize('berries');
        $this->assertEquals('berry', $result);

        $result = AkInflector::Singularize('knives');
        $this->assertEquals('knife', $result);

        $result = AkInflector::Singularize('scarves');
        $this->assertEquals('scarf', $result);

        $result = AkInflector::Singularize('men');
        $this->assertEquals('man', $result);

        $result = AkInflector::Singularize('women');
        $this->assertEquals('woman', $result);

        $result = AkInflector::Singularize('illuminatae');
        $this->assertEquals('illuminatae', $result);
    }

    public function testTitleize()
    {
        $result = AkInflector::Titleize('this-is-a-string');
        $this->assertEquals('This Is A String', $result);

        $result = AkInflector::Titleize('this-is-a-string', 'first');
        $this->assertEquals('This is a string', $result);

        $result = AkInflector::Titleize('this_is_a_string');
        $this->assertEquals('This Is A String', $result);

        $result = AkInflector::Titleize('thisIsString');
        $this->assertEquals('This Is String', $result);
    }

    public function testCamelize()
    {
        $result = AkInflector::Camelize('this-is-a-string');
        $this->assertEquals('ThisIsAString', $result);

        $result = AkInflector::Camelize('this_is_a_string');
        $this->assertEquals('ThisIsAString', $result);

        $result = AkInflector::Camelize('thisIsString');
        $this->assertEquals('ThisIsString', $result);
    }

    public function testUnderscore()
    {
        $result = AkInflector::Underscore('this-is-a-string');
        $this->assertEquals('this_is_a_string', $result);

        $result = AkInflector::Underscore('This Is A String');
        $this->assertEquals('this_is_a_string', $result);

        $result = AkInflector::Underscore('thisIsAString');
        $this->assertEquals('this_is_a_string', $result);
    }

    public function testDash()
    {
        $result = AkInflector::Dash('this_is_a_string');
        $this->assertEquals('this-is-a-string', $result);

        $result = AkInflector::Dash('This Is A String');
        $this->assertEquals('this-is-a-string', $result);

        $result = AkInflector::Dash('thisIsAString');
        $this->assertEquals('this-is-a-string', $result);
    }

    public function testHumanize()
    {
        $result = AkInflector::Humanize('this-is-a-string');
        $this->assertEquals('this is a string', $result);

        $result = AkInflector::Humanize('this-is-a-string', 'first');
        $this->assertEquals('This is a string', $result);

        $result = AkInflector::Humanize('this-is-a-string', 'all');
        $this->assertEquals('This Is A String', $result);

        $result = AkInflector::Humanize('this_is_a_string');
        $this->assertEquals('this is a string', $result);

        $result = AkInflector::Humanize('thisIsAString');
        $this->assertEquals('this is a string', $result);
    }

    public function testOrdinalize()
    {
        $result = AkInflector::Ordinalize(0);
        $this->assertEquals('0th', $result);

        $result = AkInflector::Ordinalize(1);
        $this->assertEquals('1st', $result);

        $result = AkInflector::Ordinalize(2);
        $this->assertEquals('2nd', $result);

        $result = AkInflector::Ordinalize(3);
        $this->assertEquals('3rd', $result);

        $result = AkInflector::Ordinalize(4);
        $this->assertEquals('4th', $result);

        $result = AkInflector::Ordinalize(10);
        $this->assertEquals('10th', $result);

        $result = AkInflector::Ordinalize(11);
        $this->assertEquals('11th', $result);

        $result = AkInflector::Ordinalize(12);
        $this->assertEquals('12th', $result);

        $result = AkInflector::Ordinalize(13);
        $this->assertEquals('13th', $result);

        $result = AkInflector::Ordinalize(21);
        $this->assertEquals('21st', $result);

        $result = AkInflector::Ordinalize(22);
        $this->assertEquals('22nd', $result);

        $result = AkInflector::Ordinalize(23);
        $this->assertEquals('23rd', $result);

        $result = AkInflector::Ordinalize(24);
        $this->assertEquals('24th', $result);

        $result = AkInflector::Ordinalize(100);
        $this->assertEquals('100th', $result);

        $result = AkInflector::Ordinalize(101);
        $this->assertEquals('101st', $result);

        $result = AkInflector::Ordinalize(102);
        $this->assertEquals('102nd', $result);

        $result = AkInflector::Ordinalize(103);
        $this->assertEquals('103rd', $result);
    }
}
