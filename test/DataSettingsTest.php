<?php
/** @noinspection PhpUnhandledExceptionInspection */

namespace Sibertec\Data\Test;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Sibertec\Data\Controllers\DataSettings;

#[CoversClass(DataSettings::class)]
class DataSettingsTest extends TestCase
{
    public function testSettings()
    {
        $settings = DataSettings::Settings(null, true);
        $this->assertEquals('SQLite', $settings->Driver);
    }
}
