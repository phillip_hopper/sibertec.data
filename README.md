Sibertec.Data is a small, easy to use PHP library that manages connections to MySQL or SQLite databases, and also can act as an Object Relational Mapper.

# How to Use

### The data settings are read from a YAML file.

You can either pass the path to the YAML file to the constructor, or set the path in a global variable, `$sibertec_settings_file`;

The settings can be in a file with other settings, or in a YAML file by themselves.

For MySQL data, use the following example.
```yaml
database:
  driver: MySQL
  server: localhost
  database: your_database
  user: your_username
  pwd: your_password
```

For SQLite data, use the following example. Only the local file system is supported.
```yaml
database:
  driver: SQLite
  server: local_file_system
  database: /path/to/your/data_file.sqlite
  user: not-used
  pwd: not-used
```

### How to use

You can pass the full path to the SQL fiole to load, or put your SQL files in a directory, and set the location with either
`define('SQL_DIR', '/path/to/sql');` or `$sibertec_sql_dir = '/path/to/sql';`.

Load an object from the database using a SQL file.
```injectablephp
$sibertec_settings_file = '/var/www/sites/your-site/settings.yaml'
$sibertec_sql_dir = '/var/www/sites/your-site/sql';

$db = AppData::MainDatabase();

$obj = SqlHelper::SqlFileToScalarObject('get_one.sql', $db);
```

Execute a SQL statement;
```injectablephp
$sibertec_settings_file = '/var/www/sites/your-site/settings.yaml'

$db = AppData::MainDatabase();

$db->run_sql("delete from your_table where id < 101");
```

Execute a SQL statement, returning an integer;
```injectablephp
$sibertec_settings_file = '/var/www/sites/your-site/settings.yaml'

$db = AppData::MainDatabase();

$val = $db->execute_scalar_int("select count(*) from your_table");
```
