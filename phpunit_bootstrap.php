<?php
/** @noinspection PhpUnhandledExceptionInspection */

use Sibertec\Data\Controllers\DataSettings;
use Sibertec\Data\Helpers\MiscFunctions;
use Sibertec\Data\Helpers\StringHelper;

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include_once __DIR__ . '/vendor/autoload.php';

global $sibertec_settings_file, $sibertec_sql_dir;

if (!defined('PROJ_DIR')) define('PROJ_DIR', __DIR__);
if (!defined('TEST_DIR')) define('TEST_DIR', PROJ_DIR . '/test');
if (!defined('TEST_RESOURCES')) define('TEST_RESOURCES', TEST_DIR . '/resources');

$sibertec_settings_file = TEST_DIR . '/config/app_settings.yaml';
$sibertec_sql_dir = TEST_RESOURCES;


initData();


function initData(): void
{
    print PHP_EOL. 'Initializing SQLite databases...' . PHP_EOL . PHP_EOL;

    $data_dir = '/tmp/sibertec_data_test';
    if (is_dir($data_dir))
        MiscFunctions::ClearDirectory($data_dir, false);

    if (!is_dir($data_dir))
        mkdir($data_dir, 0777, true);

    $settings = DataSettings::Settings();
    $db = new SQLite3($settings->Database);

    // get data definition queries
    $pattern = StringHelper::PathCombine(TEST_DIR, 'sqlite', '*.sql');

    foreach (glob($pattern) as $file_name) {

        // split the file contents into individual queries
        $sqls = array_filter(preg_split('/;\n/m', file_get_contents($file_name)), function ($s) {
            return !empty(trim($s));
        });

        foreach ($sqls as $sql) {
            $stmt = $db->prepare($sql);
            if ($stmt === false)
                throw new Exception($db->lastErrorMsg(), $db->lastErrorCode());
            $stmt->execute();
        }
    }
}
