<?php


namespace Sibertec\Data\Interfaces;


/**
 * Interface IDataSettings
 *
 * @property string server
 * @property string user
 * @property string pwd
 * @property string database
 * @property string driver  SQLite or MySQL (default MySQL)
 *
 * @package Sibertec\Data\Interfaces
 */
interface IDataSettings
{
}
