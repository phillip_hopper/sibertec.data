<?php

namespace Sibertec\Data\Abstracts;

use stdClass;

abstract class DataAccess
{
    public ?string $cache_dir;

    protected ?string $disabled_cache_dir;

    /**
     * The number of seconds cache is valid
     *
     * @var int
     */
    public int $CacheDuration = 7200;  // one day = 86400 seconds, one hour = 3600 seconds

    public abstract function __destruct();

    public abstract static function Get_Database($data_settings, $cache_dir = null): DataAccess;

    /**
     * Closes the database connection and releases resources
     */
    public abstract function Close_Database();

    /**
     * Returns the name of the selected database
     *
     * @return string
     */
    public abstract function DatabaseName(): string;

    /**
     * Set the timezone for queries
     *
     * @param string $timezone_name_or_offset Name like 'America/New_York' or offset like '-05:00'
     */
    public abstract function SetTimezone(string $timezone_name_or_offset): void;

    /**
     * Executes the SQL statement and returns a pointer to the results
     *
     * @param string $sql
     * @param bool $run_as_one
     * @return bool
     */
    public abstract function open_recordset(string $sql, bool $run_as_one = true): bool;

    /**
     * Returns the next record as an array
     *
     * @param boolean $just_names
     * @return array|null
     */
    public abstract function fetch_next_record(bool $just_names = false): ?array;

    /**
     * Returns the next record as an object
     *
     * @return stdClass|null
     */
    public abstract function fetch_next_object(): ?stdClass;

    /**
     * Executes the SQL statement
     *
     * @param string $sql
     */
    public abstract function run_sql(string $sql): void;

    /**
     * Executes the SQL statement and returns the first row as an array
     *
     * @param string $sql
     *
     * @return mixed|null
     */
    public abstract function execute_scalar(string $sql): mixed;

    /**
     * Executes the SQL statement and returns the first row as an object
     *
     * @param string $sql
     *
     * @return stdClass|null
     */
    public abstract function execute_scalar_object(string $sql): ?stdClass;

    /**
     * Executes the SQL statement and returns the first column of the first row as an int
     *
     * @param string $sql
     *
     * @return int|null
     */
    public function execute_scalar_int(string $sql): ?int
    {
        $return_val = $this->execute_scalar($sql);
        settype($return_val, 'integer');
        return $return_val;
    }

    /**
     * Executes the SQL statement and returns the first column of the first row as a float
     *
     * @param string $sql
     *
     * @return float|null
     */
    public function execute_scalar_float(string $sql): ?float
    {
        $return_val = $this->execute_scalar($sql);
        settype($return_val, 'float');
        return $return_val;
    }

    /**
     * Executes the SQL statement and returns the first column of the first row as a bool
     *
     * @param string $sql
     *
     * @return bool|null
     */
    public function execute_scalar_bool(string $sql): ?bool
    {
        $return_val = $this->execute_scalar($sql);
        settype($return_val, 'boolean');
        return $return_val;
    }

    /**
     * Executes the SQL statement and returns the first column of the first row as a string
     *
     * @param string $sql
     *
     * @return string|null
     */
    public function execute_scalar_string(string $sql): ?string
    {
        $return_val = $this->execute_scalar($sql);
        settype($return_val, 'string');
        return $return_val;
    }

    /**
     * Turn off the database cache
     */
    public function DisableCache(): void
    {
        if (!empty($this->cache_dir)) {
            $this->disabled_cache_dir = $this->cache_dir;
            $this->cache_dir = null;
        }
    }

    /**
     * Turn database caching back on
     */
    public function EnableCache(): void
    {
        if (empty($this->cache_dir) && !empty($this->disabled_cache_dir)) {
            $this->cache_dir = $this->disabled_cache_dir;
        }
    }
}
