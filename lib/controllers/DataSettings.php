<?php

namespace Sibertec\Data\Controllers;

use Exception;
use Sibertec\Data\Helpers\MiscFunctions;
use Sibertec\Data\Interfaces\IDataSettings;

class DataSettings
{
    protected static ?DataSettings $instance;

    public string $Server;
    public string $User;
    public string $Pwd;
    public string $Database;
    public string $Driver;   // SQLite or MySQL (default MySQL)

    /**
     * @param string|null $file_name
     * @throws Exception
     */
    protected function __construct(string $file_name = null)
    {
        global $sibertec_settings_file;

        if (empty($file_name)) {

            if (empty($sibertec_settings_file))
                return;

            $file_name = $sibertec_settings_file;
        }

        $settings = MiscFunctions::YamlDecodeFile($file_name, false);

        if (empty($settings->database))
            return;

        /** @var IDataSettings $db_settings */
        $db_settings = $settings->database;

        $this->Server = $db_settings->server;
        $this->User = $db_settings->user;
        $this->Pwd = $db_settings->pwd;
        $this->Database = $db_settings->database;
        $this->Driver = $db_settings->driver;
    }

    /**
     * @param string|null $file_name
     * @param bool|null $reset
     * @return DataSettings
     * @throws Exception
     */
    public static function Settings(string $file_name = null, bool $reset = null): DataSettings
    {
        if ($reset)
            self::$instance = null;

        if (!empty($file_name))
            self::$instance = new DataSettings($file_name);

        if (empty(self::$instance))
            self::$instance = new DataSettings();

        return self::$instance;
    }
}
