<?php

namespace Sibertec\Data\Controllers;

use Exception;
use Sibertec\Data\Abstracts\DataAccess;
use Sibertec\Data\Data\Database;
use Sibertec\Data\Data\SQLiteDatabase;
use Sibertec\Data\Helpers\DateHelper;

class AppData
{
    private static ?DataAccess $main_database = null;
    private static array $drupal_database = [];

    /**
     * @return DataAccess
     * @throws Exception
     */
    public static function MainDatabase(): DataAccess
    {
        if (!self::ConnectionIsOpen(self::$main_database))
            self::$main_database = self::ConnectToDatabase(DataSettings::Settings());

        return self::$main_database;
    }

    /**
     * @param string $db_name
     * @return DataAccess
     * @throws Exception
     */
    public static function DrupalDatabase(string $db_name = 'default'): DataAccess
    {
        if (empty(self::$drupal_database[$db_name]) || !self::ConnectionIsOpen(self::$drupal_database[$db_name]))
            self::$drupal_database[$db_name] = self::ConnectToDrupalDatabase($db_name);

        return self::$drupal_database[$db_name];
    }

    /**
     * @param string $db_name
     * @return DataAccess
     * @throws Exception
     */
    public static function ConnectToDrupalDatabase(string $db_name = 'default'): DataAccess
    {
        if (!class_exists('Drupal\Core\Database\Database'))
            throw new Exception('Drupal database class not found.');

        $settings = \Drupal\Core\Database\Database::getConnectionInfo($db_name);
        if (empty($settings) || empty($settings['default']))
            throw new Exception('Drupal default database settings for "' . $db_name . '" not found.');

        $settings = $settings['default'];

        $data_settings = DataSettings::Settings();

        if (strtolower($settings['driver']) == 'sqlite') {
            $data_settings->Driver = 'SQLite';
            $data_settings->Server = 'local_file_system';
            $data_settings->Database = $settings['database'];
            $data_settings->User = 'user';
            $data_settings->Pwd =  'password';
        }
        else {
            $data_settings->Driver = 'MySQL';
            $data_settings->Server = $settings['host'];
            $data_settings->Database = $settings['database'];
            $data_settings->User = $settings['username'];
            $data_settings->Pwd =  $settings['password'];
        }

        return self::ConnectToDatabase($data_settings);
    }

    /**
     * @param DataSettings $data_settings
     * @return DataAccess
     * @throws Exception
     * @noinspection PhpMissingParamTypeInspection
     */
    private static function ConnectToDatabase($data_settings): DataAccess
    {
        if (strtolower($data_settings->Driver ?? 'mysql') == 'sqlite') {
            $db = SQLiteDatabase::Get_Database($data_settings);
        }
        else {
            $db = Database::Get_Database($data_settings);
            $db->cn->set_charset('utf8mb4');
            $db->run_sql("SET time_zone = '" . DateHelper::GetTimezoneOffset() . "';");
            $db->run_sql("SET collation_connection = 'utf8mb4_unicode_ci';");
            $db->run_sql('SET SESSION wait_timeout=120');  // 120 seconds = 2 minutes
        }

        return $db;
    }

    /**
     * Can be used for unit testing.
     *
     * @param DataAccess $db
     * @return void
     */
    public static function SetTestDatabase(DataAccess $db): void
    {
        self::$main_database = $db;
    }

    private static function ConnectionIsOpen($db): bool
    {
        if (empty($db) || empty($db->cn) || $db->cn->stat() === false)
            return false;

        return true;
    }
}
