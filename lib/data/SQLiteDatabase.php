<?php

namespace Sibertec\Data\Data;

use Exception;
use Sibertec\Data\Abstracts\DataAccess;
use Sibertec\Data\Controllers\DataSettings;
use SQLite3;
use SQLite3Result;
use stdClass;

class SQLiteDatabase extends DataAccess
{
    public ?SQLiteDatabase $cn = null;
    public ?SQLite3Result $rs = null;

    private ?SQLite3 $sqlite3_instance = null;
    private bool $disposed = false;
    private ?string $dbn;

    /**
     * Hold instances of the class
     * @var SQLiteDatabase[]
     */
    private static array $instances = array();

    private function __construct()
    {
    }

    /**
     * Returns an instance of this class
     *
     * @param DataSettings $data_settings
     * @param string|null $cache_dir
     * @return SQLiteDatabase
     * @throws Exception
     */
    public static function Get_Database($data_settings, $cache_dir = null): SQLiteDatabase
    {
        if (empty($data_settings->Database))
            throw new Exception('Database setting not found');

        $db_key = 'db_' . $data_settings->Database;

        // check if there is already a connection, and if the connection is still open
        if (!array_key_exists($db_key, self::$instances)) {

            $c = __CLASS__;
            self::$instances[$db_key] = new $c();
        }

        // set cache directory
        if (!empty($cache_dir)) {
            if (substr_compare($cache_dir, DIRECTORY_SEPARATOR, -1) === 0) {
                $cache_dir = substr($cache_dir, 0, -1);
            }
            self::$instances[$db_key]->cache_dir = $cache_dir;
        }

        // open the connection
        self::$instances[$db_key]->open_database($data_settings->Database);

        return self::$instances[$db_key];
    }

    /**
     * @param string $database_file_name
     * @return void
     * @throws Exception
     */
    private function open_database(string $database_file_name): void
    {
        if (!empty($this->sqlite3_instance))
            return;

        $this->sqlite3_instance = new SQLite3($database_file_name);
        if ($this->sqlite3_instance->lastErrorCode())
            throw new Exception($this->sqlite3_instance->lastErrorMsg(), $this->sqlite3_instance->lastErrorCode());

        $this->dbn = $database_file_name;
        $this->cn = $this;
    }

    public function __destruct()
    {
        $this->dispose();
    }

    protected function dispose(): void
    {
        if (!$this->disposed) {
            if (!is_null($this->rs)) $this->close_recordset();
            if (!is_null($this->sqlite3_instance)) $this->Close_Database();
            $this->disposed = true;
        }
    }

    /**
     * @inheritDoc
     */
    public function Close_Database(): void
    {
        $this->close_recordset();
        if (!is_null($this->sqlite3_instance)) {
            $this->sqlite3_instance->close();
            $this->sqlite3_instance = null;
            $this->cn = null;
        }
    }

    /**
     * @inheritDoc
     */
    public function DatabaseName(): string
    {
        return $this->dbn;
    }

    /**
     * @inheritDoc
     */
    public function SetTimezone(string $timezone_name_or_offset): void
    {
        // TODO: Implement SetTimezone() method.
    }

    /**
     * @param string $sql
     * @param bool $run_as_one
     * @return bool
     * @throws Exception
     */
    public function open_recordset(string $sql, bool $run_as_one = true): bool
    {
        $this->rs = $this->multi_query($sql);
        return true;
    }

    /**
     * @param string $sql
     * @return SQLite3Result|null
     * @throws Exception
     */
    private function multi_query(string $sql): ?SQLite3Result
    {
        $return_val = null;

        $this->close_recordset();

        if (stripos($sql, 'DELIMITER') !== false) {
            $sqls = [$sql];
        }
        else {
            $sqls = array_filter(preg_split('/;\n/m', $sql), function ($s) {
                return !empty(trim($s));
            });
        }

        foreach ($sqls AS $s) {

            // remove the database name prefix from user function names
            $s = preg_replace('/[^\r\n\t\f\v ()]+\.(uf\S+\()/', '$1', $s);

            // remove # comments
            $s = preg_replace('/^#.*$/m', "\n", $s);

            // ON DUPLICATE KEY UPDATE
            $s = preg_replace('/ON\s+DUPLICATE\s+KEY\s+UPDATE/i', 'ON CONFLICT DO UPDATE SET', $s);

            // VALUES function for the ON DUPLICATE KEY UPDATE clause
            if (str_contains($s, 'ON CONFLICT DO UPDATE SET')) {
                $parts = explode('ON CONFLICT DO UPDATE SET', $s);
                $parts[1] = preg_replace('/VALUES\(([^)]+)\)/i', 'excluded.$1', $parts[1]);
                $s = implode('ON CONFLICT DO UPDATE SET', $parts);
            }

            // BIT value, should just be 0 or 1
            $s = preg_replace('/b\'(\d+)\'/i', '$1', $s);

            // LAST_INSERT_ID function, change to last_insert_rowid
            $s = preg_replace('/last_insert_id\(\)/i', 'last_insert_rowid()', $s);

            // IF function, change to IIF
            $s = preg_replace('/(\s|\()IF\(/mi', '$1IIF(', $s);

            // SEPARATOR for GROUP_CONCAT
            $s = preg_replace('/ SEPARATOR ([^)]+\))/mi', ' , $1', $s);

            // INSERT IGNORE
            $s = preg_replace('/INSERT\sIGNORE/i', 'INSERT OR IGNORE', $s);

            // drop and create tables
            $drop_re = '/drop( temporary)? table/i';
            $create_re = '/create( temporary)? table/i';
            if (preg_match($drop_re, $s)) {
                $s = preg_replace($drop_re, 'DROP TABLE', $s);
            }
            elseif (preg_match($create_re, $s)) {
                $s = preg_replace($create_re, 'CREATE TABLE', $s);

                // remove `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci`
                $s = preg_replace('/\)[^)]*$/i', ')', $s);
            }

            $stmt = $this->sqlite3_instance->prepare($s);
            if ($stmt === false)
                throw new Exception($this->sqlite3_instance->lastErrorMsg(), $this->sqlite3_instance->lastErrorCode());
            $return_val = $stmt->execute();
            if ($return_val === false)
                throw new Exception($this->sqlite3_instance->lastErrorMsg(), $this->sqlite3_instance->lastErrorCode());
        }

        return $return_val;
    }

    private function close_recordset(): void
    {
        if (is_null($this->rs))
            return;

        $this->rs->finalize();
        $this->rs = null;
    }

    /**
     * @inheritDoc
     */
    public function fetch_next_record(bool $just_names = false): ?array
    {
        if (!is_null($this->rs)) {
            $result = $this->rs->fetchArray($just_names ? SQLITE3_ASSOC : SQLITE3_BOTH);

            if ($result === false)
                return null;

            return $result;
        }

        // no result set open
        return null;
    }

    /**
     * @inheritDoc
     */
    public function fetch_next_object(): ?stdClass
    {
        if (!is_null($this->rs)) {
            $result = $this->rs->fetchArray(SQLITE3_ASSOC);

            if ($result === false)
                return null;

            return (object)$result;
        }

        // no result set open
        return null;
    }

    /**
     * @param string $sql
     * @return void
     * @throws Exception
     */
    public function run_sql(string $sql): void
    {
        $this->multi_query($sql);
    }

    /**
     * @param string $sql
     * @return mixed
     * @throws Exception
     */
    public function execute_scalar(string $sql): mixed
    {
        // initialize
        $return_val = null;

        // execute the query
        $this->open_recordset($sql);
        $temp = $this->fetch_next_record();

        if ($temp)
            $return_val = $temp[0];

        // clean up
        $this->close_recordset();

        // return the first value
        return $return_val;
    }

    /**
     * @param string $sql
     * @return stdClass|null
     * @throws Exception
     */
    public function execute_scalar_object(string $sql): ?stdClass
    {
        // initialize
        $return_val = null;

        // execute the query
        $this->open_recordset($sql);
        $temp = $this->fetch_next_object();

        if ($temp) $return_val = $temp;

        // clean up
        $this->close_recordset();

        // return the first value
        return $return_val;
    }

    public function stat(): bool|string
    {
        if (empty($this->sqlite3_instance) || empty($this->dbn))
            return false;

        return 'connected';
    }

    /** @noinspection PhpUnusedParameterInspection
     * @noinspection PhpUnused
     */
    public function set_charset(string $charset): bool
    {
        return true;
    }

    public function escape_string(string $val): string
    {
        return Sqlite3::escapeString($val);
    }

    /** @noinspection PhpUnused */
    public function real_escape_string(string $val): string
    {
        return $this->escape_string($val);
    }

    /** @noinspection PhpUnused */
    public function createFunction(string $name, callable $callback, int $argCount = -1, int $flags = 0): bool
    {
        if (empty($this->sqlite3_instance))
            return false;

        return $this->sqlite3_instance->createFunction($name, $callback, $argCount, $flags);
    }
}
