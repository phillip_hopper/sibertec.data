<?php

namespace Sibertec\Data\Data;

use Exception;
use stdClass;

class Collection extends stdClass
{
    protected ?array $items;
    public ?array $ChangedItems;

    public function __construct()
    {
        $this->items = array();
        $this->ChangedItems = array();
    }

    public function __destruct()
    {
        $this->items = null;
        $this->ChangedItems = null;

        unset($this->items);
        unset($this->ChangedItems);
    }

    /**
     * Used by the magic getter
     * @param $key
     *
     * @return mixed
     */
    public function Get($key): mixed
    {
        return $this->items[$key];
    }

    /**
     * Used by the magic setter
     *
     * @param $key
     * @param $value
     */
    public function Set($key, $value): void
    {
        // check if the value actually changed
        if (array_key_exists($key, $this->items))
            if ($this->items[$key] === $value) return;

        // remember it has changed, but not if it starts with 2 underscores
        if (!str_starts_with($key, '__') && !array_key_exists($key, (array) $this->ChangedItems))
            $this->ChangedItems[$key] = 1;

        // set the new value
        $this->items[$key] = $value;
    }

    /**
     * Sets or replaces the entire list at once
     * @param array $list
     */
    public function SetList(array $list): void
    {
        $this->items = $list;
    }

    /**
     * Adds an item to the list by key
     *
     * @param $key
     * @param $value
     */
    public function Add($key, $value): void
    {
        $this->Set($key, $value);
    }

    /**
     * Is this value in the collection?
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function ContainsValue(mixed $value): bool
    {
        return (in_array($value, (array) $this->items));
    }

    /**
     * Is this key in the collection?
     *
     * @param int|string $key
     *
     * @return bool
     */
    public function ContainsKey(int|string $key): bool
    {
        return (array_key_exists($key, (array) $this->items));
    }

    /**
     * Returns the array holding the collection items
     *
     * @return array|null
     */
    public function GetCollectionAsArray(): ?array
    {
        return $this->items;
    }

    /**
     * Returns the concatenated collection
     *
     * @param string $delimiter
     *
     * @return string
     */
    public function GetCollectionAsString(string $delimiter = ', '): string
    {
        return implode($delimiter, $this->items);
    }

    /**
     * Inserts an item at the specified index
     *
     * @param int $index
     * @param mixed $value
     */
    public function Insert(int $index, mixed $value): void
    {
        $this->items[$index] = $value;
    }

    /**
     * Remove an item from the collection.
     *
     * @param $value
     *
     * @throws Exception
     */
    public function Remove($value): void
    {
        $index = array_search($value, $this->items);
        if ($index === false)
            throw new Exception('The value wa not found in the collection, so it could not be removed.', 0);

        self::RemoveAt($index);
    }

    /**
     * Remove an item by array key
     *
     * @param int|string $key
     *
     * @throws Exception
     */
    public function RemoveAt(int|string $key): void
    {
        if (array_key_exists($key, $this->items))
            unset($this->items[$key]);

        if (array_key_exists($key, $this->ChangedItems))
            unset($this->ChangedItems[$key]);
    }

    /**
     * Remove a range of items
     *
     * @param int $startIndex
     * @param int $endIndex
     *
     * @throws Exception
     */
    public function RemoveRange(int $startIndex, int $endIndex): void
    {
        for ($i = $startIndex; $i < $endIndex; $i++) {
            self::RemoveAt($i);
        }
    }

    /**
     * Sort the collection
     */
    public function Sort(): void
    {
        sort($this->items, SORT_STRING);
    }

    /**
     * Get the number of items in the collection
     *
     * @return int
     */
    public function Count(): int
    {
        return count($this->items);
    }

    /**
     * Has the collection changed?
     *
     * @return bool
     */
    public function HasChanged(): bool
    {
        return (count($this->ChangedItems) > 0);
    }

    /**
     * Reset the change tracker
     */
    public function ClearChangedFlags(): void
    {
        $this->ChangedItems = array();
    }

}
