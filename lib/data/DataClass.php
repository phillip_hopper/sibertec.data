<?php

namespace Sibertec\Data\Data;

use Serializable;
use Sibertec\Data\Abstracts\DataAccess;

/**
 * Class DataClass
 *
 * @property string created_by
 * @property string updated_by
 *
 * @package RVW\Core\Data
 */
class DataClass extends Collection implements Serializable
{

    protected bool $delete = false;
    protected ?string $table_name;

    public ?Database $db;
    public bool $allow_zero_id = false;

    /**
     * DataClass constructor.
     *
     * @param string|null $table_name
     * @param DataAccess|null $db
     */
    function __construct(string $table_name = null, DataAccess $db = null)
    {
        parent::__construct();
        $this->table_name = $table_name;
        $this->db = $db;
    }

    public function serialize(): ?string
    {
        return serialize($this->items);
    }

    public function unserialize($data): void
    {
        $this->SetList(unserialize($data));
    }

    public function __serialize(): array
    {
        return ['items' => $this->items];
    }

    public function __unserialize($data)
    {
        $this->SetList($data['items']);
    }

    /**
     * The magic setter
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, mixed $value)
    {
        $this->Set($name, $value);
    }

    /**
     * The magic getter
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->Get($name);
    }

    /**
     * The magic isset function
     *
     * @param string $key
     *
     * @return bool
     */
    public function __isset(string $key)
    {
        return $this->ContainsKey($key);
    }

    /**
     * Sets whether or not this record should be deleted
     *
     * @param bool $delete_me
     */
    public function ToBeDeleted(bool $delete_me): void
    {
        $this->delete = $delete_me;
        $this->Set('__to_be_deleted', $delete_me);
    }

    /**
     * Does a diff, returning the items of this object that are not in $other
     *
     * @param DataClass $other
     * @return array
     */
    public function Diff(DataClass $other): array
    {
        return array_diff_assoc($this->GetCollectionAsArray(), $other->GetCollectionAsArray());
    }

    protected function Save($db, $table_name, $id_field_name, $new_id_value = null, $is_new_record = false): void
    {
        if ($this->delete) {
            $this->deleteMe($db, $table_name, $id_field_name, $this->Get($id_field_name));
        } else {
            if ($this->HasChanged()) {

                $existingRecord = false;

                if ($this->ContainsKey($id_field_name)) {
                    $id_val = $this->Get($id_field_name);
                    if (!empty($id_val) || ($id_val == 0 && $this->allow_zero_id)) {
                        if (!$is_new_record) {
                            $existingRecord = true;
                        }
                    }
                }

                if ($existingRecord) {
                    // this is an existing record
                    $this->saveExisting($db, $table_name, $id_field_name, $this->Get($id_field_name));
                } else {
                    // this is a new record
                    $this->saveNew($db, $table_name, $id_field_name, $new_id_value);
                }
            }
        }
    }

    /**
     *
     * @param Database $db
     * @param string $table_name
     * @param string $id_field_name
     * @param mixed|null $id_value
     */
    private function saveNew(Database $db, string $table_name, string $id_field_name, mixed $id_value = null): void
    {
        // if the id value is passed, set it now
        if ($id_value) {
            $this->Set($id_field_name, $id_value);
        }

        $table_fixed = $this->formatTableName($table_name);

        $sql1  = "insert into $table_fixed (";
        $sql2  = 'values (';
        $delim = '';

        foreach (array_keys($this->ChangedItems) as $key) {

            // do not save if the field name starts with a double-underscore
            if (substr_compare($key, '__', 0, 2) != 0) {

                $val = $this->Get($key);

                $sql1 .= "$delim `$key`";

                if (is_null($val)) {
                    $sql2 .= "$delim NULL";
                } elseif ($val === true) {
                    $sql2 .= "$delim b'1'";
                } elseif ($val === false) {
                    $sql2 .= "$delim b'0'";
                } else {
                    $val = $db->cn->escape_string($val);
                    $sql2 .= "$delim '$val'";
                }

                // set the delimiter (,) after the first item
                if ($delim == '') {
                    $delim = ',';
                }
            }
        }

        $sql1 .= ')';
        $sql2 .= ');';

        $sql = $sql1 . ' ' . $sql2;
        $db->run_sql($sql);

        // get the new id
        if ($id_value == null) {
            $sql = 'select last_insert_id();';
            $new_id = $db->execute_scalar_int($sql);
            if ($new_id > 0) {
                $this->Set($id_field_name, $new_id);
                $this->ClearChangedFlags();
            }
        }
    }

    /**
     *
     * @param DataAccess $db
     * @param string $table_name
     * @param string $id_field_name
     * @param mixed $id_value
     */
    private function saveExisting(DataAccess $db, string $table_name, string $id_field_name, mixed $id_value): void
    {
        $table_fixed = $this->formatTableName($table_name);

        $sql = "update $table_fixed set";
        $delim = '';

        foreach (array_keys($this->ChangedItems) as $key) {

            // do not save if the field name starts with a double-underscore
            if (substr_compare($key, '__', 0, 2) != 0) {

                $val = $this->Get($key);

                if (is_null($val)) {
                    $sql .= "$delim $key = NULL";
                } elseif ($val === true) {
                    $sql .= "$delim $key = b'1'";
                } elseif ($val === false) {
                    $sql .= "$delim $key = b'0'";
                } else {
                    $val = $db->cn->escape_string($val);
                    $sql .= "$delim `$key` = '$val'";
                }

                // set the delimiter (,) after the first item
                if ($delim == '') {
                    $delim = ',';
                }
            }
        }

        $sql .= " where `$id_field_name` = '$id_value';";
        $db->run_sql($sql);
        $this->ClearChangedFlags();
    }

    /**
     * @param Database $db
     * @param string $table_name
     * @param string $id_field_name
     * @param string|int $id_value
     */
    private function deleteMe(Database $db, string $table_name, string $id_field_name, string|int $id_value): void
    {
        $table_fixed = $this->formatTableName($table_name);

        /** @noinspection SqlResolve */
        $sql = "delete from $table_fixed where `$id_field_name` = '$id_value';";
        $db->run_sql($sql);
    }

    /**
     * This is a default Save function. Derived classes should override it.
     *
     * @param DataAccess $db
     * @param string|null $updated_by
     */
    public function SaveMe(DataAccess $db, string $updated_by = null): void
    {
        if ($this->HasChanged() || $this->delete) {

            if ($this->delete) {
                $this->Save($db, $this->table_name, 'id');
                return;
            }

            if (!empty($updated_by)) {
                if (empty($this->id))
                    $this->created_by = $updated_by;
                else
                    $this->updated_by = $updated_by;
            }

            $this->Save($db, $this->table_name, 'id');
        }
    }

    private function formatTableName($table_name): string
    {
        $table_name = str_replace('`', '', $table_name);
        $parts = explode('.', $table_name);
        return '`' . implode('`.`', $parts) . '`';
    }
}
