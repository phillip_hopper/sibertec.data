<?php

namespace Sibertec\Data\Data;

use Sibertec\Data\Abstracts\DataAccess;
use stdClass;

class DataCollection extends Collection
{
    protected ?DataAccess $db = null;  // the database where the collection resides
    private string $collection_type;
    public string $sql = '';   // the sql statement for the collection
    public array $params;     // the parameters for the SQL statement above

    /**
     * DataCollection constructor.
     * @param string $data_class_type
     * @param DataAccess|null $db
     */
    function __construct(string $data_class_type, DataAccess $db = null)
    {
        parent::__construct();
        $this->collection_type = $data_class_type;
        if ($db)
            $this->db = $db;
    }

    function load_collection(): void
    {
        $cache_file_name = '';
        $rows = null;

        if (!empty($this->db->cache_dir)) {
            $cache_file_name = hash('haval192,3', $this->sql) . '.cache';
            $rows = $this->get_from_data_cache($cache_file_name);
        }

        if (empty($rows)) {

            // get the records
            $this->db->open_recordset($this->sql);
            $rows = $this->recordset_to_array($this->db);

            // save to cache
            if (!empty($this->db->cache_dir)) {
                $this->save_to_data_cache($rows, $cache_file_name);
            }
        }

        // list to hold the DataClass objects
        $my_list = array();

        // loop through the records
        foreach ($rows as $row) {

            $cls = $this->object_from_row($row);

            // add the DataClass to the list
            $my_list[] = $cls;
        }

        // set the list of DataClasses for this DataCollection
        $this->SetList($my_list);
    }

    /**
     * @return DataClass|null
     */
    function get_one_object(): ?DataClass
    {
        $cache_file_name = '';

        if (!empty($this->db->cache_dir)) {

            $cache_file_name = hash('haval192,3', $this->sql) . '.single.cache';
            $contents = $this->get_from_data_cache($cache_file_name);

            if ($contents !== null)
                return $this->object_from_row($contents);
        }

        // get the records
        $this->db->open_recordset($this->sql);

        // get the record
        $row = $this->db->fetch_next_record(true);
        if ($row) {

            // save to cache
            if (!empty($this->db->cache_dir))
                $this->save_to_data_cache($row, $cache_file_name);

            // create a new instance of the DataClass
            return $this->object_from_row($row);
        }

        return null;
    }

    function AddObject($object): void
    {
        $this->items[] = $object;
    }

    protected function object_from_row($row): DataClass
    {
        $keys = array_keys($row);

        // list for this DataClass instance
        $lst = array();

        // set the data values, using column names for keys
        for ($i = 0; $i < count($row); $i++) {
            $lst[$keys[$i]] = $row[$keys[$i]];
        }

        /* @var $cls DataClass */

        // create a new instance of the DataClass
        $cls = new $this->collection_type();

        // set the list for the DataClass
        $cls->SetList($lst);
        $cls->ClearChangedFlags();
        $cls->db = $this->db;

        return $cls;
    }

    /**
     * Gets all the objects from the database.
     * @param DataAccess $db
     * @param string $class_name
     * @param string $sql_file
     * @return DataCollection
     */
    protected static function Get_All(DataAccess $db, string $class_name, string $sql_file): DataCollection
    {
        $sql = file_get_contents($sql_file);

        $col = new DataCollection($class_name, $db);
        $col->sql = $sql;
        $col->load_collection();

        return $col;
    }

    /**
     * Get an object by ID
     * @param DataAccess $db
     * @param string $class_name If empty($class_name) == true, a stdClass object is returned
     * @param string $sql_file
     * @param int $id
     * @return DataClass|null
     */
    protected static function Get_By_Object_ID(DataAccess $db, string $class_name, string $sql_file, int $id): ?DataClass
    {
        $sql = file_get_contents($sql_file);
        $sql = str_replace('@id', mysqli_real_escape_string($db->cn, $id), $sql);

        return self::get_one_by_sql($db, $class_name, $sql);
    }

    /**
     * Get a collection of child objects
     * @param DataAccess $db
     * @param string $class_name
     * @param string $sql_file
     * @param int $parent_id Used to replace `@parent_id` in the $sql_file
     * @return DataCollection
     */
    protected static function Get_By_Parent_ID(DataAccess $db, string $class_name, string $sql_file, int $parent_id): DataCollection
    {
        $sql = file_get_contents($sql_file);
        $sql = str_replace('@parent_id', mysqli_real_escape_string($db->cn, $parent_id), $sql);

        $col = new DataCollection($class_name, $db);
        $col->sql = $sql;

        $col->load_collection();

        return $col;
    }

    /**
     *
     * @param DataAccess $db
     * @param string $class_name
     * @param string $sql_file
     * @param int $parent_id Used to replace `@parent_id` in the $sql_file
     * @return DataClass|null
     */
    protected static function Get_First_By_Parent_ID(DataAccess $db, string $class_name, string $sql_file, int $parent_id): ?DataClass
    {
        $sql = file_get_contents($sql_file);
        $sql = str_replace('@parent_id', mysqli_real_escape_string($db->cn, $parent_id), $sql);

        return self::get_one_by_sql($db, $class_name, $sql);
    }

    /**
     * Return the first object found
     * @param DataAccess $db
     * @param string $class_name
     * @param string $sql_file
     * @return DataClass|null
     */
    protected static function Get_First(DataAccess $db, string $class_name, string $sql_file): ?DataClass
    {
        $sql = file_get_contents($sql_file);
        return self::get_one_by_sql($db, $class_name, $sql);
    }

    /**
     * Return the first object found
     * @param DataAccess $db
     * @param string $class_name
     * @param string $sql
     * @return DataClass|null
     */
    private static function get_one_by_sql(DataAccess $db, string $class_name, string $sql): ?DataClass
    {

        /* @var $col DataCollection */
        /* @var $obj DataClass */
        $col = new DataCollection($class_name, $db);
        $col->sql = $sql;
        return $col->get_one_object();
    }

    /**
     * @param $cacheFileName
     * @return array|null
     */
    protected function get_from_data_cache($cacheFileName): ?array
    {
        // if the file does not exist, return null
        $cacheFileName = $this->db->cache_dir . DIRECTORY_SEPARATOR . $cacheFileName;

        if (!is_file($cacheFileName)) return null;

        // check if expired
        $fileCreated = filemtime($cacheFileName);
        if (($fileCreated + $this->db->CacheDuration) < time()) {
            unlink($cacheFileName);
            return null;
        }

        $contents = file_get_contents($cacheFileName);

        // return the cached results as an array
        if ($contents !== false)
            return json_decode($contents, true);

        // return null if not in cache
        return null;
    }

    protected function save_to_data_cache($contents, $cacheFileName): void
    {
        $cacheFileName = $this->db->cache_dir . DIRECTORY_SEPARATOR . $cacheFileName;

        $cache_dir = dirname($cacheFileName);

        // we are doing this so the cache can be cleared by a user other than www-data
        $old_mask = umask(0);

        if (!is_dir($cache_dir))
            mkdir($cache_dir, 0775, true);

        file_put_contents($cacheFileName, json_encode($contents));

        umask($old_mask);
    }

    /**
     * @param DataAccess $db
     * @return array
     */
    protected static function recordset_to_array(DataAccess $db): array
    {
        $return_val = array();

        while ($row = $db->fetch_next_record(true)) {
            $return_val[] = $row;
        }

        return $return_val;
    }

    /**
     * Converts the collection of DataClass objects to stdClass objects (for passing to Twig template))
     * @return stdClass[]
     */
    public function GetCollectionAsArrayOfStdClass(): array
    {
        $return_val = [];

        /** @var Collection $item */
        foreach ($this->items as $item) {
            $return_val[] = (object)$item->GetCollectionAsArray();
        }

        return $return_val;
    }
}
