<?php

namespace Sibertec\Data\Data;

use mysqli;
use mysqli_result;
use Sibertec\Data\Abstracts\DataAccess;
use Sibertec\Data\Controllers\DataSettings;
use stdClass;

/**
 *
 * Database Class for MySQL Server
 *
 * Database dependent
 *
 */
class Database extends DataAccess
{
    public ?mysqli $cn = null;
    public ?mysqli_result $rs = null;

    private string $dbn;
    private bool $disposed = false;

    /**
     * Hold instances of the class
     * @var Database[]
     */
    private static array $instances = array();

    private static string $default_timezone_offset;

    /**
     * @param string $dbn Database name
     */
    private function __construct(string $dbn)
    {
        $this->dbn = $dbn;
    }

    public function __destruct()
    {
        $this->dispose();
    }

    protected function dispose(): void
    {
        if (!$this->disposed) {
            if (!is_null($this->rs)) $this->close_recordset();
            if (!is_null($this->cn)) $this->Close_Database();
            $this->disposed = true;
        }
    }

    /**
     * Returns an instance of this class
     *
     * @param DataSettings $data_settings
     * @param string|null $cache_dir
     * @return Database
     */
    public static function Get_Database($data_settings, $cache_dir = null): Database
    {
        $db_key = 'db_' . $data_settings->Server . '_' . $data_settings->Database;

        // check if there is already a connection, and if the connection is still open
        if (!array_key_exists($db_key, self::$instances) || empty(self::$instances[$db_key]->cn) || self::$instances[$db_key]->cn->stat() === false) {

            $c = __CLASS__;
            self::$instances[$db_key] = new $c($data_settings->Database);
        }

        // set cache directory
        if (!empty($cache_dir)) {
            if (substr_compare($cache_dir, DIRECTORY_SEPARATOR, -1) === 0) {
                $cache_dir = substr($cache_dir, 0, -1);
            }
            self::$instances[$db_key]->cache_dir = $cache_dir;
        }

        // open the connection
        self::$instances[$db_key]->open_database($data_settings->Server, $data_settings->User, $data_settings->Pwd);

        return self::$instances[$db_key];
    }

    private function open_database($svr, $uid, $pwd): void
    {
        if (!empty($this->cn)) return;

        $this->cn = new mysqli($svr, $uid, $pwd);
        if ($this->cn->connect_errno) trigger_error($this->cn->connect_error, E_USER_ERROR);

        $this->cn->set_charset('utf8');
        $this->SetTimezone(self::getDefaultTimezoneOffset());

        if (!empty($this->dbn)) $this->select_db($this->dbn);
    }

    private function select_db($dbn): void
    {
        if (!$this->cn->select_db($dbn)) trigger_error($this->cn->error, E_USER_ERROR);
    }

    /**
     * Closes the database connection and releases resources
     */
    public function Close_Database(): void
    {
        $this->close_recordset();
        if (!is_null($this->cn)) {
            $this->cn->close();
            $this->cn = null;
        }
    }

    /**
     * Executes the SQL statement and returns a pointer to the results
     *
     * @param string $sql
     * @param bool $run_as_one
     * @return bool
     */
    public function open_recordset(string $sql, bool $run_as_one = true): bool
    {
        $this->close_recordset();

        if ($run_as_one) {
            $sqls = [$sql];
        }
        else {
            $sqls = array_filter(preg_split('/;\n/m', $sql), function ($s) {
                return !empty(trim($s));
            });
        }

        foreach ($sqls AS $s) {
            $stmt = $this->cn->prepare($s);
            if ($stmt === false) trigger_error($this->cn->error, E_USER_ERROR);
            $result = $stmt->execute();
            if ($result === false) trigger_error($this->cn->error, E_USER_ERROR);
            $this->rs = $stmt->get_result() or trigger_error($this->cn->error, E_USER_ERROR);
        }

        return true;
    }

    /**
     * Executes the SQL statement and returns the first row as an array
     *
     * @param string $sql
     *
     * @return mixed|null
     */
    public function execute_scalar(string $sql): mixed
    {
        // initialize
        $return_val = null;

        // execute the query
        $this->open_recordset($sql);
        $temp = $this->fetch_next_record();

        if ($temp)
            $return_val = $temp[0];

        // clean up
        $this->close_recordset();

        // return the first value
        return $return_val;
    }

    /**
     * Executes the SQL statement and returns the first row as an object
     *
     * @param string $sql
     *
     * @return stdClass|null
     */
    public function execute_scalar_object(string $sql): ?stdClass
    {
        // initialize
        $return_val = null;

        // execute the query
        $this->open_recordset($sql);
        $temp = $this->fetch_next_object();

        if ($temp) $return_val = $temp;

        // clean up
        $this->close_recordset();

        // return the first value
        return $return_val;
    }

    private function close_recordset(): void
    {
        do {
            if (!is_null($this->rs)) {
                $this->rs->free();
                $this->rs = null;
            }

            if ($this->cn->more_results()) $this->cn->next_result();
        } while ($this->cn->more_results());
    }

    /**
     * Returns the next record as an array
     *
     * @param boolean $just_names
     * @return array|null
     */
    public function fetch_next_record(bool $just_names = false): ?array
    {
        if (!is_null($this->rs))
            return $this->rs->fetch_array($just_names ? MYSQLI_ASSOC : MYSQLI_BOTH);

        // no result set open
        return null;
    }

    /**
     * Returns the next record as an object
     *
     * @return stdClass|null
     */
    public function fetch_next_object(): ?stdClass
    {
        if (!is_null($this->rs))
            return $this->rs->fetch_object();

        // no result set open
        return null;
    }

    /**
     * Executes the SQL statement
     *
     * @param string $sql
     */
    public function run_sql(string $sql): void
    {
        $this->close_recordset();
        $result = $this->cn->multi_query($sql);
        if ($result === false) trigger_error($this->cn->error, E_USER_ERROR);
        $this->cn->store_result();
    }

    /**
     * Returns the name of the selected database
     *
     * @return string
     */
    public function DatabaseName(): string
    {
        return $this->dbn;
    }

    /**
     * Set the timezone for queries
     *
     * @param string $timezone_name_or_offset Name like 'America/New_York' or offset like '-05:00'
     */
    public function SetTimezone(string $timezone_name_or_offset): void
    {
        $this->cn->multi_query("SET time_zone = '$timezone_name_or_offset';");
    }

    private static function getDefaultTimezoneOffset(): string
    {
        if (empty(self::$default_timezone_offset)) {
            $seconds    = (int)date('Z');
            $plus_minus = ($seconds >= 0) ? '+' : '-';
            $hours      = abs($seconds / 3600);
            $minutes    = abs($seconds % 3600) / 60;

            self::$default_timezone_offset = sprintf("%s%02d:%02d", $plus_minus, $hours, $minutes);
        }

        return self::$default_timezone_offset;
    }
}
