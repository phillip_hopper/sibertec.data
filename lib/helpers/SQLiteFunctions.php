<?php

namespace Sibertec\Data\Helpers;

use Exception;

class SQLiteFunctions
{
    /**
     * SQLite does not have a built-in CONCAT function
     *
     * @param mixed ...$values
     * @return string|null
     */
    public static function CONCAT(mixed ...$values): ?string
    {
        // returns null if any of the values are null
        $null_values = array_filter($values, fn($value) => is_null($value));
        if (!empty($null_values))
            return null;

        return implode('', $values);
    }

    public static function IF_function($condition, $true_value, $false_value): mixed
    {
        return $condition ? $true_value : $false_value;
    }

    /**
     * SQLite does not have a built-in CONCAT_WS function
     *
     * @param string|null $sep
     * @param mixed ...$values
     * @return string|null
     */
    public static function CONCAT_WS(?string $sep, mixed ...$values): ?string
    {
        // returns NULL if the separator is null
        if (is_null($sep))
            return null;

        // skips null values in the values to concat
        $values = array_filter($values, fn ($value) => !is_null($value));

        return implode($sep, $values);
    }

    /**
     * SQLite does not have a built-in DATEDIFF function
     *
     * @param $expr1
     * @param $expr2
     * @return int|null
     */
    public static function DATEDIFF($expr1, $expr2): ?int
    {
        if (empty($expr1) || empty($expr2))
            return null;

        $date1 = date_create(DateHelper::DateTimeStrToSQL($expr1));
        $date2 = date_create(DateHelper::DateTimeStrToSQL($expr2));
        $interval = date_diff($date2, $date1);

        return $interval->days * ($interval->invert ? -1: 1);
    }

    /**
     * SQLite does not have a built-in DAYOFWEEK function
     *
     * @param $date
     * @return int|null
     */
    public static function DAYOFWEEK($date): ?int
    {
        if (empty($date))
            return null;

        $test_date = DateHelper::DateStrToSQL($date);

        return intval(date('w', strtotime($test_date))) + 1;
    }

    /**
     * @param $unix_timestamp
     * @param string|null $format
     * @return string|null
     * @noinspection PhpUnusedParameterInspection
     */
    public static function FROM_UNIXTIME($unix_timestamp, string $format = null): ?string
    {
        if (is_numeric($unix_timestamp))
            $unix_timestamp = intval($unix_timestamp);

        return DateHelper::DateTimeStrToSQL($unix_timestamp);
    }

    /**
     * SQLite does not have a built-in GREATEST function
     *
     * @param mixed ...$values
     * @return string|null
     */
    public static function GREATEST(mixed ...$values): ?string
    {
        // returns null if any of the values are null
        $null_values = array_filter($values, fn($value) => is_null($value));
        if (!empty($null_values))
            return null;

        return max($values);
    }

    public static function NOW(): ?string
    {
        return DateHelper::DateTimeStrToSQL('now');
    }

    public static function SUBSTRING_INDEX($str, $delim, $count): ?string
    {
        if (!isset($str, $delim, $count))
            return null;

        if ($count < 0)
            return implode($delim, array_slice(explode($delim, $str), $count));

        return implode($delim, array_slice(explode($delim, $str), 0, $count));
    }

    public static function REGEXP($pattern, $data): ?int
    {
        if (!isset($pattern, $data))
            return null;

        $pattern = '/' . $pattern . '/mi';
        return intval(preg_match($pattern, $data));
    }

    public static function TRUNCATE($value, $num): ?float
    {
        if (!isset($value, $num))
            return null;

        return floor($value * pow(10, $num)) / pow(10, $num);
    }

    public static function YEAR($date): ?string
    {
        $year = strtotime($date);
        return date('Y', $year);
    }

    public static function MONTH($date): ?string
    {
        $month = strtotime($date);
        return date('n', $month);
    }

    public static function CURDATE(): ?string
    {
        return DateHelper::DateStrToSQL('now');
    }

    public static function CURRENT_DATE(): ?string
    {
        return self::CURDATE();
    }

    public static function FIND_IN_SET($needle, $haystack): ?int
    {
        if (is_null($needle) || is_null($haystack))
            return null;

        if (empty($needle) || empty($haystack))
            return 0;

        $list = explode(',', $haystack);
        $found = array_search($needle, $list);

        if ($found === false)
            return 0;

        return $found + 1;
    }

    public static function CHAR_LENGTH($value): ?int
    {
        if (is_null($value))
            return null;

        return mb_strlen($value);
    }

    public static function COMPRESS($value): string
    {
        return (string)$value;
    }

    public static function UNCOMPRESS($value): string
    {
        return (string)$value;
    }

    public static function TO_BASE64($value): string
    {
        return base64_encode($value);
    }

    /**
     * @return int
     * @throws Exception
     */
    public static function RAND(): int
    {
        return random_int(1, PHP_INT_MAX);
    }
}
