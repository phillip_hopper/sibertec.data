<?php

namespace Sibertec\Data\Helpers;

use Exception;
use Sibertec\Data\Abstracts\DataAccess;
use stdClass;

class SqlHelper
{
    /**
     * @param $file_name
     * @return string
     */
    public static function GetFromFile($file_name): string
    {
        global $sibertec_sql_dir;

        if (is_file($file_name))
            return file_get_contents($file_name);


        // first check for a project version of the file
        if (defined('SQL_DIR')) {
            $test_file = StringHelper::PathCombine(SQL_DIR, $file_name);
            if (is_file($test_file))
                return file_get_contents($test_file);
        }

        if (!empty($sibertec_sql_dir)) {
            $test_file = StringHelper::PathCombine($sibertec_sql_dir, $file_name);
            if (is_file($test_file))
                return file_get_contents($test_file);
        }

        return '';
    }

    /**
     * @param string $file_name_or_sql
     * @param DataAccess $db
     * @param array|null $params
     * @param bool $do_not_escape
     * @param bool $right_trim
     * @return string
     * @throws Exception
     */
    public static function LoadSQL(string $file_name_or_sql, DataAccess $db, ?array $params=null, bool $do_not_escape=false, bool $right_trim=false): string
    {
        $sql = self::GetFromFile($file_name_or_sql);

        // if the file was not found, maybe an SQL statement was passed
        if (empty($sql) && !StringHelper::EndsWith(strtolower($file_name_or_sql), '.sql'))
            $sql = $file_name_or_sql;

        // process includes
        $sql = self::ProcessIncludes($sql);

        if (empty($sql)) {
            if (defined('DEBUG'))
                throw new Exception('SQL file not found: ' . $file_name_or_sql);
            else
                return '';
        }

        if ($right_trim)
            $sql = rtrim($sql, "; \t\n\r\0\x0B");

        if (empty($params))
            return $sql;

        return self::Prepare($db, $sql, $params, $do_not_escape);
    }

    public static function Prepare(DataAccess $db, string $sql, array $params, bool $do_not_escape=false): string|null
    {
        if (empty($params))
            return $sql;

        // was an associative array passed?
        $string_keys = array_filter(array_keys($params), 'is_string');

        // if no string keys found, use vsprintf
        if (empty($string_keys)) {
            $clean_args = array_map(function($s) use($db) {

                if (is_null($s))
                    return 'NULL';

                return '\'' . $db->cn->real_escape_string($s) . '\'';
                }, $params);
            return vsprintf($sql, $clean_args);
        }

        foreach($params as $key => $value) {

            if (is_null($value)) {
                $sql = preg_replace('/\'?' . $key . '\'?/m', 'NULL', $sql);
            }
            elseif ($do_not_escape) {
                $sql = str_replace($key, $value, $sql);
            }
            else {
                $sql = str_replace($key, $db->cn->real_escape_string($value), $sql);
            }
        }

        return $sql;
    }

    /**
     *
     * @param DataAccess $db
     * @param bool $add_row_num
     *
     * @return array
     */
    public static function RecordsetToArray(DataAccess $db, bool $add_row_num=false): array
    {
        $return_val = array();

        $i = 1;
        while ($row = $db->fetch_next_object()) {

            if ($add_row_num)
                $row->row_num = $i++;

            $return_val[] = $row;
        }

        return $return_val;
    }

    /**
     * @param DataAccess $db
     * @param bool $add_row_num
     * @return array
     */
    public static function RecordsetToAssociativeArray(DataAccess $db, bool $add_row_num=false): array
    {
        $return_val = array();

        $i = 1;
        while ($row = $db->fetch_next_record(true)) {

            if ($add_row_num)
                $row['row_num'] = $i++;

            $return_val[] = $row;
        }

        return $return_val;
    }

    /**
     * @param string $file_name_or_sql
     * @param DataAccess $db
     * @param array|null $params
     * @param bool $do_not_escape
     * @param bool $add_row_num
     *
     * @return array
     * @throws Exception
     */
    public static function SqlFileToArray(string $file_name_or_sql, DataAccess $db, ?array $params=null, bool $do_not_escape=false, bool $add_row_num=false): array
    {
        $sql = self::LoadSQL($file_name_or_sql, $db, $params, $do_not_escape);
        $db->open_recordset($sql);
        return self::RecordsetToArray($db, $add_row_num);
    }

    /**
     * @param string $file_name_or_sql
     * @param DataAccess $db
     * @param array|null $params
     * @param bool $do_not_escape
     * @param bool $add_row_num
     * @return array
     * @throws Exception
     */
    public static function SqlFileToAssociativeArray(string $file_name_or_sql, DataAccess $db, ?array $params=null, bool $do_not_escape=false, bool $add_row_num=false): array
    {
        $sql = self::LoadSQL($file_name_or_sql, $db, $params, $do_not_escape);
        $db->open_recordset($sql);
        return self::RecordsetToAssociativeArray($db, $add_row_num);
    }

    /**
     * @param string $file_name_or_sql
     * @param DataAccess $db
     * @param array|null $params
     * @param bool $do_not_escape
     *
     * @return stdClass|null
     * @throws Exception
     */
    public static function SqlFileToScalarObject(string $file_name_or_sql, DataAccess $db, ?array $params=null, bool $do_not_escape=false): ?stdClass
    {
        $sql = self::LoadSQL($file_name_or_sql, $db, $params, $do_not_escape);
        return $db->execute_scalar_object($sql);
    }

    public static function FloorplanDateToSQL($value): string
    {
        $parts = explode('-', $value);

        return $parts[2] . '-' . str_pad($parts[0], 2, '0', STR_PAD_LEFT) . '-' . str_pad($parts[1], 2, '0', STR_PAD_LEFT);
    }

    private static function ProcessIncludes(string $sql): string
    {
        $sql = preg_replace_callback(
            '/^@include\s+(\S+?)$/m',
            function ($matches) {

                global $sibertec_sql_dir;

                $include = $matches[1];

                if (!empty($sibertec_sql_dir)) {
                    $test_file = StringHelper::PathCombine($sibertec_sql_dir, 'include', trim($include));
                    if (is_file($test_file))
                        return file_get_contents($test_file);
                }

                // @codeCoverageIgnoreStart
                if (defined('SQL_DIR')) {
                    $test_file = StringHelper::PathCombine(SQL_DIR, 'include', trim($include));
                    if (is_file($test_file))
                        return file_get_contents($test_file);
                }
                // @codeCoverageIgnoreEnd

                return '';
            },
            $sql
        );

        return trim($sql);
    }
}
