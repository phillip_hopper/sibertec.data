<?php

namespace Sibertec\Data\Helpers;

class StringHelper
{

    /**
     * Determine if it is possible for the haystack to contain the needle
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    private static function may_contain(string $haystack, string $needle): bool
    {
        $haystack_len = strlen($haystack);
        $needle_len = strlen($needle);

        if ($haystack_len < 1)
            return false;

        if ($needle_len < 1)
            return false;

        return !($needle_len > $haystack_len);
    }

    public static function BeginsWith($haystack, $needle): bool
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return (str_starts_with($haystack, $needle));
    }

    /**
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public static function BeginsWithAny(string $haystack, array $needles): bool
    {
        foreach($needles as $needle) {
            if (self::BeginsWith($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    public static function EndsWith($haystack, $needle): bool
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return substr_compare($haystack, $needle, -(strlen($needle))) === 0;
    }

    public static function Contains($haystack, $needle): bool
    {
        if (!self::may_contain($haystack, $needle))
            return false;

        if ($haystack == $needle)
            return true;

        return (str_contains($haystack, $needle));
    }

    public static function ContainsAny(string $haystack, array $needles): bool
    {
        foreach($needles as $needle) {
            if (self::Contains($haystack, $needle))
                return true;
        }

        return false;
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR.
     *
     * @return false|string The path properly combined
     */
    public static function PathCombine(): false|string
    {
        $sep = DIRECTORY_SEPARATOR;
        $not_sep = ($sep == '/') ? '\\' : '/';

        $segments = func_get_args();

        // check if an array was passed
        if (!empty($segments) && is_array($segments[0]))
            $segments = $segments[0];

        $seg_cnt = count($segments);

        $return_val = '';

        // check for windows vs unix
        for ($i = 0; $i < $seg_cnt; $i++) {
            $segments[$i] = str_replace($not_sep, $sep, $segments[$i]);

            // remove trailing DIRECTORY_SEPARATOR from all segments
            $segments[$i] = self::RemoveLeadingAndTrailingSeparators($segments[$i], $i, $sep);

            // combine the segments
            if (!empty($segments[$i])) {

                if (empty($return_val)) {
                    $return_val = $segments[$i];
                } else {
                    $return_val .= $sep . $segments[$i];
                }
            }
        }

        return $return_val;
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR and converts to the absolute path
     *
     * @return string The canonical path, or false if path not found.
     */
    public static function RealPathCombine(): string
    {
        $args = func_get_args();
        $path = call_user_func_array('Sibertec\Data\Helpers\StringHelper::PathCombine', $args);
        return realpath($path);
    }

    /**
     * @return false|string The path properly combined
     */
    public static function UrlCombine(): false|string
    {
        $sep = '/';

        $segments = func_get_args();
        $seg_cnt = count($segments);

        $return_val = '';

        // check for windows vs unix
        for ($i = 0; $i < $seg_cnt; $i++) {

            // remove trailing SEPARATOR from all segments
            $segments[$i] = self::RemoveLeadingAndTrailingSeparators($segments[$i], $i, $sep);

            // combine the segments
            if (!empty($segments[$i])) {

                if (empty($return_val)) {
                    $return_val = $segments[$i];
                } else {
                    $return_val .= $sep . $segments[$i];
                }
            }
        }

        return $return_val;
    }

    /**
     * @param string $value
     * @param int $idx
     * @param string $sep
     *
     * @return string
     */
    private static function RemoveLeadingAndTrailingSeparators(string $value, int $idx, string $sep): string
    {
        // remove trailing SEPARATOR from all segments
        while (self::EndsWith($value, $sep)) {
            $value = substr($value, 0, strlen($value) - 1);
        }

        // remove leading DIRECTORY_SEPARATOR from all segments except the first
        if ($idx != 0) {
            while (self::BeginsWith($value, $sep)) {
                $value = substr($value, 1);
            }
        }

        return $value;
    }

    /** @noinspection PhpUndefinedFunctionInspection */
    public static function GUID(): string
    {
        if (function_exists('com_create_guid') === true)
            return trim(com_create_guid(), '{}');

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function StripPhone($value, $trim_extra=false): string
    {
        $regex = '/([^0-9])/';
        $stripped = preg_replace($regex, '', $value);

        // if the length is 11 and the number starts with 1, remove the first digit
        if ($trim_extra && (strlen($stripped) == 11) && str_starts_with($stripped, '1'))
            $stripped = substr($stripped, 1);

        return $stripped;
    }

    /**
     * Returns something like 937-843-9000 or (937) 843-9000
     *
     * @param $value
     * @param bool $use_parens
     *
     * @return string
     */
    public static function FormatPhone($value, bool $use_parens = true): string
    {
        $stripped = self::StripPhone($value, true);

        switch (strlen($stripped)) {
            case 10:
                if ($use_parens)
                    return '(' . substr($stripped, 0, 3) . ') ' . substr($stripped, 3, 3) . '-' . substr($stripped, 6);
                else
                    return substr($stripped, 0, 3) . '-' . substr($stripped, 3, 3) . '-' . substr($stripped, 6);

            case 7:
                return substr($stripped, 0, 3) . '-' . substr($stripped, 3);
        }

        // if not able to format, return the original value
        return $value;
    }

    /**
     * Formats US phone numbers for SMS. Returns something like +19378439000
     *
     * @param $value
     * @param bool $add_plus
     *
     * @return string
     */
    public static function FormatPhoneSMS($value, bool $add_plus=true): string
    {
        $stripped = self::StripPhone($value);

        switch (strlen($stripped)) {
            case 11:
                return ($add_plus ? '+' : '') . $stripped;

            case 10:
                return ($add_plus ? '+' : '') . '1' . $stripped;
        }

        // if not a full number, return what we have
        return $stripped;
    }

    /**
     * Formats a phone number for use in a regular expression. Returns something like '.*937.*843.*9000'
     *
     * @param $value
     *
     * @return string|null
     */
    public static function FormatPhoneRegex($value): ?string
    {
        $stripped = self::StripPhone($value, true);

        if (strlen($stripped) == 10) {
            return '.*' . substr($stripped, 0, 3) . '.*' . substr($stripped, 3, 3) . '.*' . substr($stripped, 6);
        }

        // if not able to format, return null
        return null;
    }

	/**
     * Convert string to lower case and escape for a URL
     *
	 * @param $value
	 *
	 * @return string
	 */
	public static function LowerEscape($value): string
    {
		return urlencode(strtolower($value));
	}

    /**
     * Convert string to lower case and escape for a URL, replacing space and underscore with a dash
     *
     * @param $value
     *
     * @return string
     */
    public static function LowerDashEscape($value): string
    {
        $value = str_replace(' ', '-', $value);
        $value = str_replace('_', '-', $value);
        return self::LowerEscape($value);
    }

    /**
     * Return the correct format of a word, singular or plural
     *
     * @param $quantity
     * @param $singular
     * @param null $plural
     *
     * @return string|null
     */
    public static function Pluralize($quantity, $singular, $plural=null): ?string
    {
        if ($quantity == 1 || !strlen($singular)) return $singular;
        if ($plural !== null) return $plural;

        return AkInflector::Pluralize($singular);
    }

    /**
     * Return the singular form of the word
     * @param $word
     * @return string
     */
    public static function Singularize($word): string
    {
        return AkInflector::Singularize($word);
    }

    public static function ProperCaseName(?string $name): string
    {
        if (empty($name))
            return '';

        if (strtolower($name) === $name)
            return self::ProperCaseString($name);
        elseif (strtoupper($name) === $name && strlen($name) > 3)
            return self::ProperCaseString(strtolower($name));

        if (!str_contains($name, ' '))
            return $name;

        $names = explode(' ', $name);
        $parts = [];
        foreach ($names as $n) {
            $parts[] = self::ProperCaseString($n);
        }

        return implode(' ', $parts);
    }

    private static function ProperCaseString(string $string): string
    {
        $val = ucwords($string, " \t\r\n\f\v-");

        // fix 'Mc' names
        $re = '/(^Mc|\sMc)([a-z])/m';
        $val = preg_replace_callback($re, function($matches) { return $matches[1] . strtoupper($matches[2]); }, $val);

        // fix "O'" names
        $re = '/(^O\'|\sO\')([a-z])/m';
        return preg_replace_callback($re, function($matches) { return $matches[1] . strtoupper($matches[2]); }, $val);
    }

    public static function IsValidEmail($email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);
    }
}
