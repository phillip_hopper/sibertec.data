<?php

namespace Sibertec\Data\Helpers;

// +----------------------------------------------------------------------+
// | Akelos Framework - http://www.akelos.org                             |
// +----------------------------------------------------------------------+
// | Copyright (c) 2002-2006, Akelos Media, S.L.  & Bermi Ferrer Martinez |
// | Released under the GNU Lesser General Public License, see LICENSE.txt|
// +----------------------------------------------------------------------+


/**
 * @package ActiveSupport
 * @subpackage Inflector
 * @author Bermi Ferrer <bermi a.t akelos c.om>
 * @copyright Copyright (c) 2002-2006, Akelos Media, S.L. http://www.akelos.org
 * @license GNU Lesser General Public License <http://www.gnu.org/copyleft/lesser.html>
 */

/**
 * AkInflector for pluralize and singularize English nouns.
 *
 * This AkInflector is a port of Ruby on Rails AkInflector.
 *
 * It can be really helpful for developers that want to
 * create frameworks based on naming conventions rather than
 * configurations.
 *
 * You can find the inflector rules in config/inflector.yml
 * To add your own inflector rules, please do so in config/inflector/mydictionary.yml
 *
 *
 * @author Bermi Ferrer Martinez <bermi a.t akelos c.om>
 * @copyright Copyright (c) 2002-2006, Akelos Media, S.L. http://www.akelos.org
 * @license GNU Lesser General Public License <http://www.gnu.org/copyleft/lesser.html>
 */
class AkInflector
{
    private static array $irregular = [
        'person' => 'people',
        'man' => 'men',
        'child' => 'children',
        'sex' => 'sexes',
        'move' => 'moves'
    ];

    private static array $uncountable = ['equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep', 'deer'];

    /**
     * Pluralizes English nouns.
     *
     * @access public
     * @static
     * @param string $word  English noun to pluralize
     * @return string|false Plural noun
     */
    public static function Pluralize(string $word): false|string
    {
        $plural = [
            '/(quiz)$/i' => '\1zes',
            '/^(ox)$/i' => '\1en',
            '/([m|l])ouse$/i' => '\1ice',
            '/(matr|vert|ind)ix|ex$/i' => '\1ices',
            '/(x|ch|ss|sh)$/i' => '\1es',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([^aeiouy]|qu)y$/i' => '\1ies',
            '/(hive)$/i' => '\1s',
            '/(?:([^f])fe|([lr])f)$/i' => '\1\2ves',
            '/sis$/i' => 'ses',
            '/([ti])um$/i' => '\1a',
            '/(buffal|tomat)o$/i' => '\1oes',
            '/(bu)s$/i' => '\1ses',
            '/(alias|status)/i'=> '\1es',
            '/(octop|vir)us$/i'=> '\1i',
            '/(ax|test)is$/i'=> '\1es',
            '/s$/i'=> 's',
            '/$/'=> 's'
        ];

        /** @noinspection DuplicatedCode */
        $lower_cased_word = strtolower($word);

        foreach(self::$uncountable as $_uncountable) {
            if (StringHelper::EndsWith($lower_cased_word, $_uncountable))
                return $word;
        }

        foreach (self::$irregular as $_plural => $_singular) {
            if (preg_match('/('.$_plural.')$/i', $word, $arr)) {
                return preg_replace('/('.$_plural.')$/i', substr($arr[0],0,1).substr($_singular,1), $word);
            }
        }

        foreach ($plural as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                return preg_replace($rule, $replacement, $word);
            }
        }

        return false;
    }

    /**
     * Singularizes English nouns.
     *
     * @access public
     * @static
     * @param string $word English noun to singularize
     * @return string Singular noun.
     */
    public static function Singularize(string $word): string
    {
        $singular = [
            '/(quiz)zes$/i' => '\1',
            '/(matr)ices$/i' => '\1ix',
            '/(vert|ind)ices$/i' => '\1ex',
            '/^(ox)en/i' => '\1',
            '/(alias|status)es$/i' => '\1',
            '/([octop|vir])i$/i' => '\1us',
            '/(cris|ax|test)es$/i' => '\1is',
            '/(shoe)s$/i' => '\1',
            '/(o)es$/i' => '\1',
            '/(bus)es$/i' => '\1',
            '/([m|l])ice$/i' => '\1ouse',
            '/(x|ch|ss|sh)es$/i' => '\1',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([lr])ves$/i' => '\1f',
            '/(tive)s$/i' => '\1',
            '/(hive)s$/i' => '\1',
            '/([^f])ves$/i' => '\1fe',
            '/(^analy)ses$/i' => '\1sis',
            '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
            '/([ti])a$/i' => '\1um',
            '/(n)ews$/i' => '\1ews',
            '/s$/i' => '',
        ];

        /** @noinspection DuplicatedCode */
        $lower_cased_word = strtolower($word);

        foreach(self::$uncountable as $_uncountable) {
            if (StringHelper::EndsWith($lower_cased_word, $_uncountable))
                return $word;
        }

        foreach (self::$irregular as $_plural => $_singular){
            if (preg_match('/('.$_singular.')$/i', $word, $arr)) {
                return preg_replace('/('.$_singular.')$/i', substr($arr[0],0,1).substr($_plural,1), $word);
            }
        }

        foreach ($singular as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                return preg_replace($rule, $replacement, $word);
            }
        }

        return $word;
    }

    /**
     * Converts an underscored or CamelCase word into a English
     * sentence.
     *
     * The titleize function converts text like "WelcomePage",
     * "welcome_page" or  "welcome page" to this "Welcome
     * Page".
     * If second parameter is set to 'first' it will only
     * capitalize the first character of the title.
     *
     * @access public
     * @static
     * @param string $word  Word to format as tile
     * @param string $uppercase  If set to 'first' it will only uppercase the
     * first character. Otherwise it will uppercase all
     * the words in the title.
     * @return string Text formatted as title
     */
    public static function Titleize(string $word, string $uppercase = ''): string
    {
        $uppercase = $uppercase == 'first' ? 'ucfirst' : 'ucwords';
        return self::TitleizeRV($uppercase(AkInflector::Humanize(AkInflector::Underscore($word))));
    }

    /**
     * Returns given word as CamelCased
     *
     * Converts a word like "send_email" to "SendEmail". It
     * will remove non alphanumeric character from the word, so
     * "who's online" will be converted to "WhoSOnline"
     *
     * @access public
     * @static
     * @param string $word  Word to convert to camel case
     * @return string UpperCamelCasedWord
     *@see Variablize
     */
    public static function Camelize(string $word): string
    {
        return str_replace(' ','', ucwords(preg_replace('/[^A-Za-z0-9]+/',' ', $word)));
    }

    /**
     * Converts a word "into_it_s_underscored_version"
     *
     * Convert any "CamelCased" or "ordinary Word" into an
     * "underscored_word".
     *
     * This can be really useful for creating friendly URLs.
     *
     * @access public
     * @static
     * @param string $word  Word to underscore
     * @return string Underscored word
     */
    public static function Underscore(string $word): string
    {
        return strtolower(preg_replace('/[^A-Za-z0-9]+/', '_',
            preg_replace('/([a-z])([A-Z])/', '\1_\2',
                preg_replace('/([A-Z]+)([A-Z][a-z])/', '\1_\2', $word))));
    }

    /**
     * Converts a word "into-it-s-dashed-version"
     *
     * Convert any "CamelCased" or "ordinary Word" into an
     * "dashed-word".
     *
     * This can be really useful for creating friendly URLs.
     *
     * @access public
     * @static
     * @param string $word  Word to underscore
     * @return string Underscored word
     */
    public static function Dash(string $word): string
    {
        return strtolower(preg_replace('/[^A-Za-z0-9]+/', '-',
            preg_replace('/([a-z])([A-Z])/', '\1-\2',
                preg_replace('/([A-Z]+)([A-Z][a-z])/', '\1-\2', $word))));
    }

    /**
     * Returns a human-readable string from $word
     *
     * Returns a human-readable string from $word, by replacing
     * underscores with a space, and by upper-casing the initial
     * character by default.
     *
     * If you need to uppercase all the words you just have to
     * pass 'all' as a second parameter.
     *
     * @access public
     * @static
     * @param string $word  String to "humanize"
     * @param string $uppercase  If set to 'all' it will uppercase all the words
     * instead of just the first one.
     * @return string Human-readable word
     */
    public static function Humanize(string $word, string $uppercase = 'none'): string
    {
        if ($uppercase == 'none')
            $uppercase = 'strtolower';
        elseif ($uppercase == 'all')
            $uppercase = 'ucwords';
        else
            $uppercase = 'ucfirst';

        $s = AkInflector::Underscore($word);
        $s = preg_replace('/_id$/', '', $s);
        $s = str_replace('_',' ', $s);
        $s = str_replace('-',' ', $s);

        return $uppercase($s);
    }

    /**
     * Converts number to its ordinal English form.
     *
     * This method converts 13 to 13th, 2 to 2nd ...
     *
     * @access public
     * @static
     * @param integer $number  Number to get its ordinal value
     * @return string Ordinal representation of given string.
     */
    public static function Ordinalize(int $number): string
    {
        if (!in_array(($number % 100), range(11, 13))) {
            switch (($number % 10)) {
                case 1:
                    return $number . 'st';

                case 2:
                    return $number . 'nd';

                case 3:
                    return $number . 'rd';
            }

        }
        return $number . 'th';
    }

    private static function TitleizeRV($string): string
    {
        return preg_replace('/\b(rv)s?\b/mi', 'RV', $string);
    }
}
