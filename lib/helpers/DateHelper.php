<?php

namespace Sibertec\Data\Helpers;

use DateInterval;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Exception;

class DateHelper
{

    /**
     * Gets the current timezone offset as '-05:00' for use in MySQL timezone
     * @return false|string
     */
    public static function GetTimezoneOffset(): bool|string
    {
        return date('P');
    }

    /**
     * @return bool
     * @throws Exception
     */
	public static function IsDST(): bool
    {
		// Check in New York
		$date = new DateTime('now', new DateTimeZone('America/New_York'));

		// offset in hours
		$offset =  $date->getOffset() / 3600;

		return $offset == -4;
	}

	/**
	 * Convert the value to a date
	 * @param mixed $value
	 * @return int|mixed
	 */
	private static function ValueToDate(mixed $value): mixed
    {
		if (is_string($value))
			$dte = strtotime($value);
		else
			$dte = $value;
		return $dte;
	}

    /**
     * Formats date and time for SQL
     * @param mixed $datetime
     * @return string|null Returns null if not successful.
     */
	public static function DateTimeStrToSQL(mixed $datetime): ?string
    {
		$val = self::ValueToDate($datetime);

		if (is_null($val) || ($val === false) || ($val == -1))
			return null;

		return date("Y-m-d H:i:s", $val);
	}

    /**
     * Formats date for SQL
     * @param mixed $date
     * @return string|null Returns null if not successful.
     */
	public static function DateStrToSQL(mixed $date): ?string
    {
		$val = self::ValueToDate($date);

		if (is_null($val) || ($val === false) || ($val == -1))
			return null;

		return date("Y-m-d", $val);
	}

	/**
	 * Returns $value expressed in the US short date format. Returns a zero-length string if $value cannot be converted to a date.
	 * @param mixed $value
	 * @return string
	 */
	public static function ToShortDateString(mixed $value): string
    {
		$dte = self::ValueToDate($value);

		if ($dte) {
			return date("n/d/Y", $dte);
		} else {
			return '';
		}
	}

	public static function ToShortDateTimeString($value): string
    {
		$dte = self::ValueToDate($value);

		if ($dte) {
			return date("n/d/Y g:i A", $dte);
		} else {
			return '';
		}
	}

    /**
     * Returns something like 2002-02-02T14:22:22Z
     *
     * @param $value
     *
     * @return false|string|null
     */
    public static function ToISO8601Zulu($value): bool|string|null
    {
        $val = self::ValueToDate($value);

        if (is_null($val) || ($val === false) || ($val == -1))
            return null;

        return gmdate('Y-m-d\TH:i:s\Z', $val);
    }

    /**
     * Returns something like 2019-10-04T11:21:32+00:00
     *
     * @return string
     * @throws Exception
     */
    public static function CurrentISOTimestamp(): string
    {
        $utc = new DateTime('now', new DateTimeZone('UTC'));
        return $utc->format(DateTimeInterface::ATOM);
    }

    /**
     * Returns $utc_date converted from UTC to EST (handles daylight-savings time correctly also).
     *
     * @param string $utc_date
     *
     * @return string
     * @throws Exception
     */
    public static function UTC_to_EST(string $utc_date): string
    {
        $tz = date_default_timezone_get();

        date_default_timezone_set('GMT');
        $d = new DateTime($utc_date);

        $d->setTimezone(new DateTimeZone('America/New_York'));

        // reset to the original time zone
        date_default_timezone_set($tz);

        return $d->format('Y-m-d g:i:s A T');
    }

    /**
     * Returns negative if $date1 is greater than $date2
     *
     * @param int|string $date1
     * @param int|string $date2
     *
     * @return float|int
     * @throws Exception
     */
    public static function DateDiff(int|string $date1, int|string $date2): float|int
    {
        if (!is_string($date1))
            $date1 = date('Y-m-d', $date1);
        if (!is_string($date2))
            $date2 = date('Y-m-d', $date2);

        $d1 = new DateTime($date1);
        $d2 = new DateTime($date2);

        /* @var $dateInterval DateInterval */
        $dateInterval = $d1->diff($d2);

        if ($dateInterval->invert)
            return $dateInterval->days * (-1);
        else
            return $dateInterval->days;
    }

    /**
     * Returns negative if $date1 is greater than $date2
     *
     * @param int|string $date1
     * @param int|string $date2
     *
     * @return int
     * @throws Exception
     */
    public static function WeekDiff(int|string $date1, int|string $date2): int
    {
        $dif = self::DateDiff($date1, $date2);
        $dif = $dif / 7;

        if ($dif > 0)
            return (int)(floor($dif));
        else
            return (int)(ceil($dif));
    }

    /**
     *
     * @param mixed $date
     * @return int|bool
     */
    public static function FirstDayOfMonth(mixed $date): int|bool
    {
        $dte = self::ValueToDate($date);
        return strtotime(date('m', $dte) . '/01/' . date('Y', $dte) . ' 00:00:00');
    }

    /**
     *
     * @param mixed $date
     * @return int|bool
     */
    public static function LastDayOfMonth(mixed $date): int|bool
    {
        $dte = self::FirstDayOfMonth($date);
        $dte = strtotime('+1 month', $dte);
        return strtotime('-1 second', $dte);
    }

    /**
     * Returns the first day of the week containing $date.
     * Week starts on Sunday.
     * @param mixed $date
     * @return false|int|mixed
     */
    public static function FirstDayOfWeek(mixed $date): mixed
    {
        $dte = self::ValueToDate($date);
        while (date('w', $dte) != 0) {
            $dte = strtotime(date('Y-m-d', $dte) . ' -1 day');
        }
        return $dte;
    }

    /**
     * Returns the last day of the week containing $date.
     * Week ends on Saturday.
     * @param mixed $date
     * @return false|int|mixed
     */
    public static function LastDayOfWeek(mixed $date): mixed
    {
        $dte = self::ValueToDate($date);
        while (date('w', $dte) != 6) {
            $dte = strtotime(date('Y-m-d', $dte) . ' +1 day');
        }
        return $dte;
    }

    /**
     * Given the number of seconds since an event, such as the difference between 2 unix timestamps,
     * returns something like 5:12:34 or 05:12:34
     *
     * @param int $sec
     * @param bool $padHours
     *
     * @return string
     */
    public static function Sec2HMS(int $sec, bool $padHours = false): string
    {
        // start with a blank string
        $hms = '';

        // do the hours first: there are 3600 seconds in an hour, so if we divide
        // the total number of seconds by 3600 and throw away the remainder, we're
        // left with the number of hours in those seconds
        $hours = intval($sec / 3600);

        // add hours to $hms (with a leading 0 if asked for)
        $hms .= ($padHours) ? str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' : $hours . ':';

        // dividing the total seconds by 60 will give us the number of minutes
        // in total, but we're interested in *minutes past the hour* and to get
        // this, we have to divide by 60 again and then use the remainder
        $minutes = intval(fmod(($sec / 60),60));

        // add minutes to $hms (with a leading 0 if needed)
        $hms .= str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':';

        // seconds past the minute are found by dividing the total number of seconds
        // by 60 and using the remainder
        $seconds = $sec % 60;

        // add seconds to $hms (with a leading 0 if needed)
        $hms .= str_pad($seconds, 2, '0', STR_PAD_LEFT);

        // done!
        return $hms;
    }

    /**
     * @param string $value Something like 'Tue 30, Apr at 12:00 PM'
     *
     * @return false|int
     */
    public static function GetWebinarDate(string $value): bool|int
    {
        // remove the day of week
        $pos = strpos($value, ' ');
        if ($pos !== false)
            $value = substr($value, $pos + 1);

        // remove the comma
        $value = str_replace(',', '', $value);

        // remove the time portion
        $value = explode('at', $value)[0];

        // start with this year
        $year = date('Y');
        $webinar_date = strtotime($value . ' ' . ($year));

        // if the date is in the future, use last year instead
        if ($webinar_date > time())
            $webinar_date = strtotime($value . ' ' . ($year - 1));

        return $webinar_date;
    }

    /**
     * @param string $value Something like '01:23:45'
     *
     * @return float|int
     */
    public static function GetWebinarMinutes(string $value): float|int
    {
        $parts = explode(':', $value);

        if (count($parts) != 3)
            return 0;

        return (intval($parts[0]) * 60) + intval($parts[1]) + round(($parts[2] / 60));
    }

    /**
     * Converts minutes since midnight to a time string
     * @param int $minutes
     * @param bool $twelve_hour_clock
     * @return string
     */
    public static function MinutesToTimeStr(int $minutes, bool $twelve_hour_clock = true): string
    {
        if ($twelve_hour_clock)
            return gmdate('g:i A', $minutes * 60);

        return gmdate('H:i', $minutes * 60);
    }
}
